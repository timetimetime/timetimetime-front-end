module.exports = {
  "extends": "airbnb",
  "env": {
    "browser": true,
  },
  "rules": {
    "react/jsx-filename-extension": false,
    "jsx-a11y/anchor-is-valid": false,
    "import/no-named-as-default": false,
    "import/no-named-as-default-member": false,
    "react/prop-types": false,
  },
};
