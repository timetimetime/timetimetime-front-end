const nodeStatic = require('node-static');

const file = new nodeStatic.Server('./dist', {
  gzip: true,
});

require('http').createServer((request, response) => {
  request.addListener('end', () => {
    file.serve(request, response, (e) => {
      if (e && (e.status === 404)) {
        file.serveFile('/index.html', 200, {}, request, response);
      }
    });
  }).resume();
}).listen(8080);
