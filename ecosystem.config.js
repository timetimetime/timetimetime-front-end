module.exports = {
  apps: [
    {
      name: 'Time front-end',
      script: './app.js',
      // instances: 0,
      // exec_mode: 'cluster',
    },
  ],
  deploy: {
    prod: {
      user: 'time',
      host: [
        {
          host: '51.15.94.103',
          port: '48488',
        },
      ],
      ref: 'origin/master',
      repo: 'git@gitlab.com:timetimetime/timetimetime-front-end.git',
      path: '/home/time/time-front-end',
      'post-deploy': 'yarn && yarn build && /usr/local/lib/npm/bin/pm2 startOrRestart ecosystem.config.js && /usr/local/lib/npm/bin/pm2 save',
    },
  },
};
