export function setCurrentCompany(companyId) {
  return {
    type: 'SET_CURRENT_COMPANY',
    companyId,
  };
}

export function setCurrentUser(userId) {
  return {
    type: 'SET_CURRENT_USER',
    userId,
  };
}

export function setEmailConfirmationRequesting(value) {
  return {
    type: 'SET_EMAIL_CONFIRMATION_REQUESTING',
    value,
  };
}
