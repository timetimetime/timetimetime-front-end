import server from '../utils/server';
import logger from '../utils/logger';

const isLoggingEnabled = true;

function log(action, service) {
  if (isLoggingEnabled) {
    const data = {
      type: 'crud',
      action,
      payload: {
        service,
      },
    };
    logger(data);
  }
}

function camelCaseToDash(myStr) {
  return myStr.replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
}

export function addModels(modelName, models) {
  return {
    type: 'ADD_MODELS',
    modelName,
    models,
  };
}

export function changeModel(modelName, model) {
  return {
    type: 'UPDATE_MODEL',
    modelName,
    model,
  };
}

export function removeModel(modelName, modelId) {
  return {
    type: 'REMOVE_MODEL',
    modelName,
    modelId,
  };
}

export const modelActionTypes = [
  'ADD_MODELS',
  'UPDATE_MODEL',
  'REMOVE_MODEL',
];


function getDefaultHeaders() {
  const accessToken = localStorage.getItem('accessToken');
  return {
    Authorization: `Bearer ${accessToken}`,
  };
}

const commonErrorHandler = (err) => {
  if (err.name === 'NotAuthenticated') {
    window.location = '/sign-in';
    return true;
  }
  throw err;
  // setTimeout(() => { throw err; });
};

export function getModel(serviceName, modelId) {
  return dispatch => server.service(camelCaseToDash(serviceName))
    .get(modelId, { headers: getDefaultHeaders() })
    .then((model) => {
      dispatch(addModels(serviceName, [model]));
      return model;
    })
    .catch(commonErrorHandler);
}

export function getModels(serviceName, params) {
  const defaultParams = { $limit: 30 };
  defaultParams['$sort[createdAt]'] = -1;
  const query = Object.assign({}, defaultParams, params);
  return dispatch => server.service(camelCaseToDash(serviceName))
    .find({ query, headers: getDefaultHeaders() })
    .then((response) => {
      dispatch(addModels(serviceName, response.data));
      return response;
    })
    .catch(commonErrorHandler);
}

export function createModel(serviceName, data, options = {}) {
  return dispatch => server.service(camelCaseToDash(serviceName))
    .create(data, { headers: getDefaultHeaders() })
    .then((createdModel) => {
      log('create', serviceName);
      if (options.afterCreate) {
        options.afterCreate(createdModel, data);
      }
      dispatch(addModels(serviceName, [createdModel]));
      return createdModel;
    })
    .catch(commonErrorHandler);
}

export function updateModel(serviceName, data, options = {}) {
  return dispatch => server.service(camelCaseToDash(serviceName))
    .patch(data.id, data, { headers: getDefaultHeaders() })
    .then((updatedModel) => {
      log('update', serviceName);
      if (options.afterUpdate) {
        options.afterUpdate(updatedModel, data);
      }
      dispatch(changeModel(serviceName, updatedModel));
      return updatedModel;
    })
    .catch(commonErrorHandler);
}

export function deleteModel(serviceName, id) {
  return dispatch => server.service(camelCaseToDash(serviceName))
    .remove(id, { headers: getDefaultHeaders() })
    .then((response) => {
      log('delete', serviceName);
      dispatch(removeModel(serviceName, id));
      return response;
    })
    .catch(commonErrorHandler);
}
