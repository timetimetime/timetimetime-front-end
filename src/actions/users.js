import server from '../utils/server';

export function authenticate(email, password) {
  return () => server.authenticate({ email, password, strategy: 'local' });
}

export function confirmEmail(code) {
  return () => server.service('email-handler').create({ code });
}

export function recoverPassword(email) {
  return () => server.service('password-recoveries').create({ email });
}

export function completePasswordRecovery(data) {
  return () => server.service('password-recovery-handler').create(data);
}
