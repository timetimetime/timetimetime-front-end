import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { I18nextProvider } from 'react-i18next';
import {
  BrowserRouter as Router,
  Route,
  Switch,
} from 'react-router-dom';
import { notification } from 'antd';
import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import 'antd/dist/antd.css';

import AuthenticationContainer from '../Authentication/AuthenticationContainer';
import RegistrationContainer from '../Registration/RegistrationContainer';
import EnterContainer from '../Enter/EnterContainer';
import CreateCompanyContainer from '../CreateCompany/CreateCompanyContainer';
import SubtaskCategories from '../SubtaskCategories/SubtaskCategoriesContainer';
import Expenses from '../Expenses/Expenses';
import Performers from '../Performers/Performers';
import Services from '../Services/Services';
import Projects from '../Projects/Projects';
import Estimation from '../Estimation/EstimationContainer';
import EmailConfirmation from '../EmailConfirmation/EmailConfirmationContainer';
import PersonalProfileContainer from '../PersonalProfile/PersonalProfileContainer';
import PaymentsContainer from '../Payments/PaymentsContainer';
import PasswordRecoveryContainer from '../PasswordRecovery/PasswordRecoveryContainer';
import SetNewPassword from '../SetNewPassword/SetNewPasswordContainer';

import i18n from '../..//utils/i18n';
import rootReducer from '../../reducers/rootReducer';

/* eslint-disable no-underscore-dangle */
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
/* eslint-enable no-underscore-dangle */
const enhancers = composeEnhancers(applyMiddleware(thunkMiddleware));
const store = createStore(rootReducer, undefined, enhancers);

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {
    window.onerror = (message, source, lineno, colno, error) => {
      // if (error.type === 'FeathersError') {
      //   return true;
      // }
      notification.error({
        message: 'Unhandled error',
        description: message,
        duration: 0,
      });
      // return false;
    };
  }
  render() {
    return (
      <Provider store={store}>
        <I18nextProvider i18n={i18n}>
          <Router>
            <Switch>
              <Route exact path="/sign-in" component={AuthenticationContainer} />
              <Route exact path="/sign-up" component={RegistrationContainer} />
              <Route exact path="/password-recovery" component={PasswordRecoveryContainer} />
              <Route exact path="/enter" component={EnterContainer} />
              <Route exact path="/create-company" component={CreateCompanyContainer} />
              <Route exact path="/email-confirmation" component={EmailConfirmation} />
              <Route exact path="/set-new-password" component={SetNewPassword} />
              <Route path="/:companyUri/profile" component={PersonalProfileContainer} />
              <Route path="/:companyUri/subscription" component={PaymentsContainer} />
              <Route path="/:companyUri/expenses" component={Expenses} />
              <Route path="/:companyUri/performers" component={Performers} />
              <Route path="/:companyUri/product-types" component={Services} />
              <Route path="/:companyUri/work-categories" component={SubtaskCategories} />
              <Route path="/:companyUri/:projectUri" component={Estimation} />
              <Route path="/:companyUri" component={Projects} />
            </Switch>
          </Router>
        </I18nextProvider>
      </Provider>
    );
  }
}
