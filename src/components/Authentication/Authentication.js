import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import {
  Form,
  Button,
  Input,
  message,
  Row,
  Col,
  Card,
} from 'antd';
import { translate } from 'react-i18next';
import OuterPageLayout from '../OuterPageLayout/OuterPageLayoutContainer';

const queryString = require('query-string');

const toSignUpStyles = { height: '33px', lineHeight: '33px' };

class Authentication extends Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: false,
      isWaitingForServerResponse: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(event) {
    event.preventDefault();
    const { t } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({ isWaitingForServerResponse: true });
        this.props.authenticate(values.email, values.password).then(() => {
          this.setState({
            authenticated: true,
            isWaitingForServerResponse: false,
          });
        }).catch((error) => {
          this.setState({ isWaitingForServerResponse: false });
          if (error.name === 'NotAuthenticated' && error.code === 401) {
            message.error(t('failure'));
          } else {
            message.error(t('form:error.unknown'));
          }
        });
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { t, location } = this.props;
    const { isWaitingForServerResponse } = this.state;
    const returnTo = queryString.parse(location.search).from;
    return (
      <OuterPageLayout page="authentication">
        <div className="container">
          <Row>
            <Col span={6} offset={9}>
              <Card title={t('signInTitle')}>
                <Form
                  layout="vertical"
                  style={{ width: '100%' }}
                  onSubmit={this.handleSubmit}
                >
                  { this.state.authenticated ? <Redirect push to={returnTo || '/enter'} /> : null }
                  <Form.Item>
                    {
                      getFieldDecorator('email', {
                        validateTrigger: 'onBlur',
                        rules: [
                          { required: true, message: t('form:error.required') },
                          { type: 'email', message: t('form:error.emailFormat') },
                        ],
                      })(<Input autoFocus placeholder={t('email')} />)
                    }
                  </Form.Item>
                  <Form.Item>
                    {
                      getFieldDecorator('password', {
                        validateTrigger: 'onBlur',
                        rules: [
                          { required: true, message: t('form:error.required') },
                        ],
                      })(<Input type="password" placeholder={t('password')} />)
                    }
                  </Form.Item>
                  <Form.Item style={{ margin: 0 }}>
                    <Link to="/password-recovery" style={toSignUpStyles}>
                      {t('toPasswordRecovery')}
                    </Link>
                    <Button
                      id="submit"
                      type="primary"
                      htmlType="submit"
                      style={{ float: 'right' }}
                      loading={isWaitingForServerResponse}
                    >
                      {t('submit')}
                    </Button>
                    {/*
                      <Button htmlType="button">
                        <Link to="/sign-up">
                          {t('toRegistration')}
                        </Link>
                      </Button>
                    */}
                  </Form.Item>
                </Form>
              </Card>
            </Col>
          </Row>
        </div>
      </OuterPageLayout>
    );
  }
}

export default translate(['auth', 'form'])(Form.create()(Authentication));
