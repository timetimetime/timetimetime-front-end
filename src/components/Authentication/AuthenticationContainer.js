import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './Authentication';
import { authenticate } from '../../actions/users';

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    authenticate,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
