import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import {
  Form,
  Button,
  Input,
  message,
  Col,
  Row,
  Card,
} from 'antd';
import { translate } from 'react-i18next';
import OuterPageLayout from '../OuterPageLayout/OuterPageLayoutContainer';

const getSlug = require('speakingurl');

class CreateCompany extends Component {
  constructor(props) {
    super(props);
    this.state = {
      created: false,
      isWaitingForServerResponse: false,
    };
    this.suggestUri = this.suggestUri.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  suggestUri(event) {
    const { setFieldsValue } = this.props.form;
    const { value } = event.target;
    const uri = getSlug(value);
    setFieldsValue({ uri });
  }
  handleSubmit(event) {
    event.preventDefault();
    const { t } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({ isWaitingForServerResponse: true });
        this.props.createCompany(values).then(() => {
          this.setState({
            created: values.uri,
            isWaitingForServerResponse: false,
          });
        }).catch((error) => {
          this.setState({ isWaitingForServerResponse: false });
          if (error.code === 400 && error.message === 'Validation error') {
            const validationError = error.errors[0];
            if (validationError.type === 'unique violation' && validationError.path === 'uri') {
              message.error(t('uriIsNotUnique'));
            }
            return true;
          } else if (error.code === 403 && error.message === 'MODELS_NUMBER_LIMIT') {
            message.warning(t('modelsNumberLimit'));
            return true;
          }
          message.error(t('form:error.unknown'));
          return error;
        });
      }
    });
  }
  render() {
    const { t } = this.props;
    const { created, isWaitingForServerResponse } = this.state;
    const {
      getFieldDecorator,
    } = this.props.form;
    return (
      <OuterPageLayout page="create-company">
        <div className="container">
          <Row>
            <Col span={6} offset={9}>
              <Card title={t('title')}>
                <Form onSubmit={this.handleSubmit} hideRequiredMark>
                  { created ? <Redirect push to={`/${created}`} /> : null }
                  <Form.Item label={t('nameInputLabel')}>
                    {
                      getFieldDecorator('name', {
                        rules: [
                          { required: true, message: t('form:error.required') },
                          { min: 2, message: t('form:error.tooShort') },
                          { max: 64, message: t('form:error.tooLong') },
                        ],
                      })(<Input autoFocus onChange={this.suggestUri} placeholder={t('nameInputPlaceholder')} />)
                    }
                  </Form.Item>
                  <Form.Item label={t('uriInputLabel')}>
                    {
                      getFieldDecorator('uri', {
                        rules: [
                          { required: true, message: t('form:error.required') },
                          { min: 2, message: t('form:error.tooShort') },
                          { max: 128, message: t('form:error.tooLong') },
                        ],
                      })(<Input placeholder={t('uriInputPlaceholder')} />)
                    }
                  </Form.Item>
                  <Form.Item style={{ margin: 0 }}>
                    <Button
                      type="primary"
                      htmlType="submit"
                      style={{ width: '100%' }}
                      loading={isWaitingForServerResponse}
                    >
                      {t('submitButton')}
                    </Button>
                  </Form.Item>
                </Form>
              </Card>
            </Col>
          </Row>
        </div>
      </OuterPageLayout>
    );
  }
}

export default translate(['createCompany', 'form'])(Form.create()(CreateCompany));
