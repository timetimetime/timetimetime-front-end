import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './CreateCompany';
import { createModel } from '../../actions/models';

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createCompany: model => createModel('companies', model),
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
