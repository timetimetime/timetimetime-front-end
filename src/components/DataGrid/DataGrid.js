import React, { Component } from 'react';
import { Table, Input, Popconfirm, Select, message, InputNumber } from 'antd';
import { translate } from 'react-i18next';

const cH = require('../../utils/collectionHelper');

const { updateElementInCollection } = cH;

const EditableCell = ({ editable, value, onChange }) => (
  <div>
    {editable
      ? <Input style={{ margin: '-5px 0' }} value={value} onChange={e => onChange(e.target.value)} />
      : value
    }
  </div>
);

const SelectCell = ({ editable, value, onChange, options, mode, placeholder }) => (
  <div>
    {editable
      ?
        <Select
          style={{ width: '100%' }}
          placeholder={placeholder}
          mode={mode}
          onChange={onChange}
          defaultValue={value}
        >
          {
            options.map(option =>
              <Select.Option
                key={option.id}
                value={option.id}
              >
                {option.name}
              </Select.Option>
            )
          }
        </Select>
      : options.filter(el => value.includes(el.id)).map(el => el.name).join(', ')
    }
  </div>
);

const NumberCell = ({ editable, value, onChange, min, max, placeholder }) => (
  <div>
    {editable
      ?
        <InputNumber
          min={min}
          max={max}
          placeholder={placeholder}
          style={{ width: '100%' }}
          onChange={onChange}
          defaultValue={value}
        />
      : value
    }
  </div>
);

class DataGrid extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editableRecord: null,
      changedRecord: null,
      data: [],
      total: 0,
      skip: 0,
      limit: 10,
      dataIsLoading: false,
      initializationInProgress: true,
    };
    this.columns = props.columns.map(column => Object.assign({}, column, {
      render: column.render ? column.render : (text, record) => this.renderColumns(text, record, column.dataIndex, column),
    }));
    const { t, actionsColumn } = props;
    if (actionsColumn === true) {
      this.columns.push({
        title: t('actionsColumn'),
        dataIndex: 'operation',
        width: '25%',
        render: (text, record) => {
          const { editable } = record;
          return (
            <div className="editable-row-operations">
              {
                editable ?
                  <span>
                    <a onClick={() => this.save(record.id)}>
                      {t('saveEditsButton')}
                    </a>
                    &nbsp;
                    <Popconfirm
                      title={t('confirmCancelMessage')}
                      okText={t('commonui:confirm')}
                      cancelText={t('commonui:cancel')}
                      onConfirm={() => this.cancel(record.id)}
                    >
                      <a>{t('сancelEditiongButton')}</a>
                    </Popconfirm>
                  </span>
                  :
                  <span>
                    <a onClick={() => this.edit(record.id)}>
                      {t('editExpenseButton')}
                    </a>
                    &nbsp;
                    <Popconfirm
                      title={t('confirmDeleteMessage')}
                      okText={t('commonui:confirm')}
                      cancelText={t('commonui:cancel')}
                      onConfirm={() => this.delete(record.id)}
                    >
                      <a href="#">{t('deleteExpenseButton')}</a>
                    </Popconfirm>
                  </span>
              }
            </div>
          );
        },
      });
    }
    this.edit = this.edit.bind(this);
    this.cancel = this.cancel.bind(this);
    this.save = this.save.bind(this);
    this.delete = this.delete.bind(this);
    this.handlePagination = this.handlePagination.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount() {
    const { limit } = this.state;
    const { getRecords } = this.props;
    getRecords({ $limit: limit }).then((response) => {
      this.setState({
        data: response.data,
        total: response.total,
        initializationInProgress: false,
      });
    });
  }
  componentWillReceiveProps(nextProps) {
    const hasNewRecords = this.props.records.length < nextProps.records.length;
    const itIsNotInitialization = this.state.initializationInProgress === false;
    const itIsNotPageLoading = this.state.dataIsLoading === false;
    if (hasNewRecords && itIsNotInitialization && itIsNotPageLoading) {
      this.handlePagination(1);
    }
  }
  edit(key) {
    const target = this.state.data.filter(item => key === item.id)[0];
    if (target) {
      this.setState({ editableRecord: target.id });
    }
  }
  handleChange(value, key, column) {
    const target = this.state.data.filter(item => key === item.id)[0];
    if (target) {
      const update = {};
      update[column] = value;
      this.setState({ changedRecord: Object.assign(this.state.changedRecord || {}, update) });
    }
  }
  save() {
    const { t } = this.props;
    const { data } = this.state;
    const update = Object.assign({ id: this.state.editableRecord }, this.state.changedRecord);
    this.props.updateRecord(update).then((updatedModel) => {
      this.setState({
        editableRecord: null,
        changedRecord: null,
        data: updateElementInCollection(updatedModel, data),
      });
      message.success(t('expenseSavedMessage'));
    }).catch((error) => {
      if (error.response) {
        message.error('form:error:unknown');
      } else {
        throw error;
      }
    });
  }
  cancel() {
    this.setState({
      editableRecord: null,
      changedRecord: null,
    });
  }
  delete(id) {
    const { t } = this.props;
    const {
      data,
      skip,
      limit,
    } = this.state;
    this.props.deleteRecord(id).then(() => {
      message.success(t('expenseDeletedMessage'));
      const currentPageNumber = (skip / limit) + 1;
      if (data.length === 1 && currentPageNumber > 1) {
        this.handlePagination(currentPageNumber - 1);
      } else if (data.length > 0) {
        this.handlePagination(currentPageNumber);
      }
    }).catch((error) => {
      if (error.response) {
        message.error('form:error:unknown');
      } else {
        throw error;
      }
    });
  }
  handlePagination(page) {
    const { getRecords } = this.props;
    const { limit } = this.state;
    this.setState({ dataIsLoading: true });
    getRecords({
      $limit: limit,
      $skip: (page * limit) - limit,
    }).then((response) => {
      this.setState({
        data: response.data,
        total: response.total,
        skip: response.skip,
        dataIsLoading: false,
      });
    });
  }
  renderColumns(value, record, column, columnDescription) {
    if (Array.isArray(value)) {
      return (
        <SelectCell
          editable={record.editable}
          value={value}
          options={columnDescription.select.options}
          mode={columnDescription.select.mode}
          placeholder={columnDescription.select.placeholder}
          onChange={val => this.handleChange(val, record.id, column)}
        />
      );
    }
    if (columnDescription.number) {
      return (
        <NumberCell
          editable={record.editable}
          value={value}
          min={columnDescription.number.min}
          max={columnDescription.number.max}
          placeholder={columnDescription.number.placeholder}
          onChange={val => this.handleChange(val, record.id, column)}
        />
      );
    }
    return (
      <EditableCell
        editable={record.editable}
        value={value}
        onChange={val => this.handleChange(val, record.id, column)}
      />
    );
  }
  render() {
    const { t } = this.props;
    const {
      editableRecord,
      changedRecord,
      data,
      total,
      skip,
      limit,
      dataIsLoading,
    } = this.state;
    const dataSource = data
      .map(el => el.id === editableRecord ? Object.assign({}, el, changedRecord, { editable: true }) : el);
    return (
      <div>
        <Table
          rowKey="id"
          columns={this.columns}
          dataSource={dataSource}
          loading={dataIsLoading}
          rowClassName="white"
          pagination={{
            current: (skip / limit) + 1,
            defaultPageSize: limit,
            total,
            onChange: this.handlePagination,
          }}
          locale={{
            emptyText: t('commonui:noData'),
          }}
        />
      </div>
    );
  }
}

DataGrid.defaultProps = {
  actionsColumn: true,
};

export default translate(['expenses', 'form', 'commonui'])(DataGrid);
