import React, { Component } from 'react';
import { translate } from 'react-i18next';
import OuterPageLayout from '../OuterPageLayout/OuterPageLayoutContainer';

const queryString = require('query-string');

const messageStyles = {
  position: 'fixed',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  fontFamily: 'monospace',
};

class EmailConfirmation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isWaitingForServerResponse: true,
      isConfirmationSuccessful: null,
    };
  }
  componentDidMount() {
    const query = queryString.parse(this.props.location.search);
    this.props.confirmEmail(query.code).then(() => {
      this.setState({
        isWaitingForServerResponse: false,
        isConfirmationSuccessful: true,
      });
    }).catch((err) => {
      console.error(err);
      this.setState({
        isWaitingForServerResponse: false,
        isConfirmationSuccessful: false,
      });
    });
  }
  render() {
    const { t } = this.props;
    const { isWaitingForServerResponse, isConfirmationSuccessful } = this.state;
    let message;
    if (isWaitingForServerResponse) {
      message = t('confirmationInProcess');
    } else if (isConfirmationSuccessful) {
      message = t('confirmationSuccess');
    } else {
      message = t('confirmationFailure');
    }
    return (
      <OuterPageLayout page="email-confirmation">
        <p style={messageStyles}>
          {message}
        </p>
      </OuterPageLayout>
    );
  }
}

export default translate(['emailConfirmation'])(EmailConfirmation);
