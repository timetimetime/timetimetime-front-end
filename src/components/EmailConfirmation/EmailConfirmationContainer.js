import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './EmailConfirmation';
import { confirmEmail } from '../../actions/users';

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    confirmEmail,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
