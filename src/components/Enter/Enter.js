import React, { Component } from 'react';
import { Select, Button, Row, Col, Card, Form, Spin } from 'antd';
import { Link, Redirect } from 'react-router-dom';
import { translate } from 'react-i18next';
import OuterPageLayout from '../OuterPageLayout/OuterPageLayoutContainer';
import StatusWrapper from '../StatusWrapper/StatusWrapper';

const jwtDecode = require('jwt-decode');

const createCompanyStyle = { height: '33px', lineHeight: '33px' };

class Enter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDataLoaded: false,
      entered: null,
    };
    this.handleEnter = this.handleEnter.bind(this);
  }
  componentDidMount() {
    const accessToken = localStorage.getItem('accessToken');
    if (accessToken) {
      const { userId } = jwtDecode(accessToken);
      this.props.getModels('roles', { userId }).then(() => {
        this.setState({ isDataLoaded: true });
      });
    } else {
      window.location = '/sign-in';
    }
  }
  handleEnter(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.resetState();
        this.setState({ entered: values.uri });
      }
    });
  }
  render() {
    const { roles, t } = this.props;
    const { getFieldDecorator } = this.props.form;
    const { entered, isDataLoaded } = this.state;
    const defaultCompanyUri = roles.length > 0 ? roles[0].company.uri : undefined;
    return (
      <OuterPageLayout page="enter">
        { this.state.entered ? <Redirect push to={`/${entered}`} /> : null }
        <div className="container">
          <Row>
            <Col span={6} offset={9}>
              <Card title={t('title')}>
                {
                  isDataLoaded ?
                    <Form
                      layout="vertical"
                      style={{ width: '100%' }}
                      onSubmit={this.handleEnter}
                    >
                      <Form.Item>
                        {
                          getFieldDecorator('uri', {
                            initialValue: defaultCompanyUri,
                            rules: [
                              { required: true, message: t('form:error.required') },
                            ],
                          })(<Select
                            style={{ width: '100%' }}
                            placeholder={t('selectPlaceholder')}
                          >
                            {
                              roles.map(role =>
                                (
                                  <Select.Option
                                    key={role.id}
                                    value={role.company.uri}
                                  >
                                    {role.company.name}
                                  </Select.Option>
                                ))
                              }
                          </Select>)
                        }
                      </Form.Item>
                      <Form.Item style={{ margin: 0 }}>
                        <Link to="/create-company" style={createCompanyStyle}>
                          {t('createCompanyButton')}
                        </Link>
                        <Button htmlType="submit" type="primary" style={{ float: 'right' }}>
                          {t('submitButton')}
                        </Button>
                      </Form.Item>
                    </Form>
                  : <StatusWrapper height="105px"><Spin /></StatusWrapper>
                }

              </Card>
            </Col>
          </Row>
        </div>
      </OuterPageLayout>
    );
  }
}

export default translate(['enter', 'form'])(Form.create()(Enter));
