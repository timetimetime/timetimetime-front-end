import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './Enter';
import { getModels } from '../../actions/models';
import { setCurrentCompany } from '../../actions/current';
import { resetState } from '../../actions/various';

function mapStateToProps(state) {
  return {
    roles: state.roles,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getModels,
    setCurrentCompany,
    resetState,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
