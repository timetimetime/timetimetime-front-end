import React, { Component } from 'react';
import { Card, Button, Row, Col, Table, Checkbox } from 'antd';
import { translate } from 'react-i18next';
import getProjectEstimation from '../../utils/project-estimation/project-estimation';

const PieChart = require('react-chartjs').Pie;

class Calculation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCalculations: false,
      disabledTasks: props.tasks.map(el => el.id),
      disablesSubtaskCategories: props.subtaskCategories.map(el => el.id),
    };
    this.getCalculations = this.getCalculations.bind(this);
  }
  componentDidMount() {
    const { getExpenses, companyId } = this.props;
    getExpenses({ companyId });
  }
  getCalculations() {
    this.setState({ showCalculations: true });
    this.props.logger({
      action: 'get_estimation',
      type: 'various',
    });
  }
  handleTaskToggle(task) {
    const update = Object.assign({}, task, { isActive: !task.isActive });
    this.props.updateTask(update);
  }
  handleCategoryToggle(subtaskCategory) {
    const { disabledSubtaskCategories } = this.props.project;
    const update = Object.assign({}, { id: this.props.project.id });
    if (disabledSubtaskCategories.includes(subtaskCategory.id)) {
      update.disabledSubtaskCategories =
        disabledSubtaskCategories.filter(el => el !== subtaskCategory.id);
    } else {
      update.disabledSubtaskCategories =
        [...disabledSubtaskCategories, subtaskCategory.id];
    }
    this.props.updateProject(update);
  }
  render() {
    const { showCalculations } = this.state;
    const { project, t } = this.props;
    let whatToRender;
    if (showCalculations) {
      const estimation = getProjectEstimation({
        project,
        tasks: this.props.tasks,
        expenses: this.props.expenses,
        performers: this.props.performers,
        subtaskCategories: this.props.subtaskCategories,
      });
      const priceChartData = [
        { value: estimation.cost.indirectExpenses, label: t('indirectExpenses') },
        { value: estimation.cost.salaries, label: t('salaries') },
        { value: estimation.cost.income, label: t('income') },
        { value: estimation.cost.taxes, label: t('taxes') },
        { value: estimation.cost.salesmanCommission, label: t('salesmanCommission') },
      ];
      let timeChartData = [];
      if (project.type === 'agile') {
        timeChartData = estimation.additional.payload.map((task) => {
          return { value: task.duration, label: task.name };
        });
      } else if (project.type === 'waterfall') {
        timeChartData = estimation.additional.payload.map((phase) => {
          return { value: phase.duration, label: phase.subtaskCategory.name };
        });
      }
      const salariesChartData = estimation.additional.performers.map((performer) => {
        return { value: performer.reward, label: `${performer.firstName} ${performer.lastName}` };
      });
      const nF = new Intl.NumberFormat();
      const salariesTableData = estimation.additional.performers.map((performer) => {
        return {
          key: performer.id,
          name: `${performer.firstName} ${performer.lastName}`,
          reward: `${nF.format(performer.reward)} ${t('units:currency.short')}`,
        };
      });
      const salariesTableColumns = [
        {
          title: t('salariesNameColumn'),
          dataIndex: 'name',
          key: 'name',
        },
        {
          title: t('salariesRewardColumn'),
          dataIndex: 'reward',
          key: 'reward',
        },
      ];
      const durationTableData = timeChartData.map((el) => {
        let duration = null;
        if (estimation.project.type === 'waterfall') {
          duration = el.value;
        } else if (estimation.project.type === 'agile') {
          duration = el.value + ((el.value / 100) * estimation.project.insurance);
        }
        return {
          key: el.label,
          step: el.label,
          duration: `${nF.format(Math.ceil(duration))} ${t('units:time.inDays')}`,
        };
      });
      const durationTableColumns = [
        {
          title: t('durationKeyColumn'),
          dataIndex: 'step',
          key: 'step',
        },
        {
          title: t('durationValueColumn'),
          dataIndex: 'duration',
          key: 'duration',
        },
      ];
      const priceTableData = priceChartData.map((el) => {
        return {
          key: el.label,
          what: el.label,
          price: `${nF.format(el.value)} ${t('units:currency.short')}`,
        };
      });
      const priceTableColumns = [
        {
          title: t('priceKeyColumn'),
          dataIndex: 'what',
          key: 'what',
        },
        {
          title: t('priceValueColumn'),
          dataIndex: 'price',
          key: 'price',
        },
      ];
      whatToRender = (
        <Card style={{ margin: '15px 0' }}>
          <Row gutter={16}>
            <Col span={8}>
              <h3>
                {t('totalsTitle')}
              </h3>
              <p>
                {t('totalCost')}: <span id="total-cost">{ nF.format(estimation.cost.total) }</span> {t('units:currency.short')}
              </p>
              <p>
                {t('totalDuration')}: <span id="total-duration">{ estimation.duration.workingDays }</span> {t('ofWorkingDays')}
                <br />~{ estimation.duration.calendarDays } {t('ofCalendarDays')}
              </p>
            </Col>
            <Col span={8}>
              <h3>
                {t('tasksActivityTitle')}
              </h3>
              {
                this.props.tasks.map(task => (
                  <div key={task.id}>
                    <Checkbox
                      checked={task.isActive}
                      onChange={() => this.handleTaskToggle(task)}
                    >
                      {task.name}
                    </Checkbox>
                  </div>
                ))
              }
            </Col>
            <Col span={8}>
              <h3>
                {t('subtaskCategoriesActivityTitle')}
              </h3>
              {
                this.props.subtaskCategories.map(subtaskCategory => (
                  <div key={subtaskCategory.id}>
                    <Checkbox
                      checked={!project.disabledSubtaskCategories.includes(subtaskCategory.id)}
                      onChange={() => this.handleCategoryToggle(subtaskCategory)}
                    >
                      {subtaskCategory.name}
                    </Checkbox>
                  </div>
                ))
              }
            </Col>
          </Row>
          <Row gutter={16} style={{ marginTop: '16px' }}>
            <Col span={8}>
              <h4>
                {t('priceFormation')}:
              </h4>
              <PieChart data={priceChartData} />
              <Table
                dataSource={priceTableData}
                columns={priceTableColumns}
                size="small"
                pagination={false}
                style={{ marginTop: '16px' }}
              />
            </Col>
            <Col span={8}>
              <h4>
                {t('durationFormation')}:
              </h4>
              <PieChart data={timeChartData} />
              <Table
                dataSource={durationTableData}
                columns={durationTableColumns}
                size="small"
                pagination={false}
                style={{ marginTop: '16px' }}
              />
            </Col>
            <Col span={8}>
              <h4>
                {t('rewardsToPay')}:
              </h4>
              <PieChart data={salariesChartData} />
              <Table
                columns={salariesTableColumns}
                dataSource={salariesTableData}
                size="small"
                pagination={false}
                style={{ marginTop: '16px' }}
              />
              {/*
                estimation.additional.performers.map(performer => (
                  <li key={performer.id}>
                    {`${performer.firstName} ${performer.lastName}: ${nF.format(performer.reward)} ${t('units:currency.short')}`}
                  </li>
                ))
              */}
            </Col>
          </Row>
        </Card>
      );
    } else {
      whatToRender = (
        <div style={{ textAlign: 'center', margin: '15px 0' }}>
          <Button id="get-calculation-button" onClick={this.getCalculations}>
            {t('getCalculationsButton')}
          </Button>
        </div>
      );
    }
    return whatToRender;
  }
}

export default translate(['estimation', 'units'])(Calculation);
