import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './Calculation';
import { getModels, updateModel } from '../../actions/models';
import logger from '../../utils/logger';

const _ = require('lodash');

function getElementNesting(el, collection, attribute, level = -1) {
  if (typeof el.level === 'undefined') {
    el.level = level + 1;
  } else {
    el.level = el.level < level ? level : el.level;
  }
  const descendants = collection.filter(e => e[attribute].includes(el.id));
  descendants.forEach(desc => getElementNesting(desc, collection, attribute, el.level));
}

function setNestingLevel(collection, attribute) {
  const beginning = collection.filter(el => el[attribute].length === 0);
  beginning.forEach((el) => {
    getElementNesting(el, collection, attribute);
  });
}

function mapStateToProps(state, ownProps) {
  const { projectId } = ownProps;
  const project = _.find(state.projects, { id: projectId });
  const projectTasks = state.tasks.filter(task => task.projectId === projectId);
  const populatedProjectTasks = projectTasks.map(task => Object.assign({}, task, {
    subtasks: state.subtasks.filter(subtask => subtask.taskId === task.id),
  }));
  const subtaskCategories = project ?
    state.subtaskCategories
      .filter(el => el.serviceId === project.serviceId)
    : [];
  setNestingLevel(subtaskCategories, 'predecessors');
  return {
    project,
    companyId: state.current.company,
    tasks: populatedProjectTasks,
    expenses: state.expenses,
    performers: state.performers,
    subtaskCategories: _.orderBy(subtaskCategories, ['level'], ['asc']),
  };
}

function mapDispatchToProps(dispatch) {
  const actions = bindActionCreators({
    getExpenses: params => getModels('expenses', params),
    updateTask: model => updateModel('tasks', model),
    updateProject: model => updateModel('projects', model),
  }, dispatch);
  return Object.assign(actions, { logger });
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
