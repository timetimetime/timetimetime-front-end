import React, { Component } from 'react';
import { Modal, Form, Input, message } from 'antd';
import { translate } from 'react-i18next';

class CreateTaskModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fieldsNumber: 1,
      touchedFields: [],
    };
    this.handleClose = this.handleClose.bind(this);
    this.handleFieldTouch = this.handleFieldTouch.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleClose() {
    this.setState({
      touchedFields: [],
      fieldsNumber: 1,
    });
    const fieldValues = {};
    fieldValues['tasks[0].name'] = '';
    this.props.form.setFieldsValue(fieldValues);
    this.props.onCancel();
  }
  handleFieldTouch(fieldNumber) {
    const { touchedFields, fieldsNumber } = this.state;
    if (!touchedFields.includes(fieldNumber)) {
      this.setState({
        touchedFields: [...touchedFields, fieldNumber],
        fieldsNumber: fieldsNumber + 1,
      });
    }
  }
  handleSubmit(event) {
    event.preventDefault();
    const { t } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { projectId } = this.props;
        const models = values.tasks
          .filter(el => typeof (el.name) !== 'undefined')
          .map(el => Object.assign({}, el, { projectId }));
        const requests = [];
        models.forEach((el) => {
          const request = this.props.createTask(el);
          requests.push(request);
        });
        Promise.all(requests).then(() => {
          message.success(t('tasksCreatedMessage'));
          this.handleClose();
        }).catch((error) => {
          if (error.code === 403 && error.message === 'MODELS_NUMBER_LIMIT') {
            message.warning(t('taskNumberLimit'));
            return true;
          }
          message.error(t('form:error.unknown'));
          return error;
        });
      }
    });
  }
  render() {
    const { t } = this.props;
    const { fieldsNumber } = this.state;
    const {
      getFieldDecorator,
    } = this.props.form;
    const fields = [];
    for (let i = 0; i < fieldsNumber; i += 1) {
      const field = (
        <Form.Item key={i}>
          {
            getFieldDecorator(`tasks[${i}].name`, {
              rules: [
                // { required: true, message: 'Enter name please' }, autoFocus={i === 0}
                { min: 1, message: 'Too short' },
                { max: 64, message: 'Too long' },
              ],
            })(<Input placeholder={t('newTaskNamePlaceholder')} onFocus={() => this.handleFieldTouch(i)} />)
          }
        </Form.Item>
      );
      fields.push(field);
    }
    return (
      <Modal
        title={t('createTasksTitle')}
        visible={this.props.isShown}
        okText={t('createTasksButton')}
        cancelText={t('cancelTasksCreation')}
        onCancel={this.handleClose}
        onOk={this.handleSubmit}
      >
        <Form layout="vertical">
          { fields }
        </Form>
      </Modal>
    );
  }
}

export default translate(['estimation', 'form'])(Form.create()(CreateTaskModal));
