import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './CreateTask';
import { createModel } from '../../actions/models';

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createTask: model => createModel('tasks', model),
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
