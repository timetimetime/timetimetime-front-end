import React, { Component } from 'react';
import { Row, Col, Spin } from 'antd';
import Tutorial from '../Tutorial/TutorialContainer';
import TaskManagement from './TaskManagement';
import ProjectSettings from './ProjectSettingsContainer';
import Calculation from './CalculationContainer';
import InnerPageLayoutContainer from '../InnerPageLayout/InnerPageLayoutContainer';
import StatusWrapper from '../StatusWrapper/StatusWrapper';

class EstimationContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isProjectLoaded: false,
    };
  }
  componentDidMount() {
    const { project, getModels, companyId } = this.props;
    const { projectUri } = this.props.match.params;
    if (!project) {
      getModels('projects', { uri: projectUri, companyId }).then(() => {
        this.setState({ isProjectLoaded: true });
      });
    } else {
      this.setState({ isProjectLoaded: true });
    }
  }
  render() {
    const { project } = this.props;
    const { isProjectLoaded } = this.state;
    return isProjectLoaded && project ? (
      <div>
        <Tutorial theme="estimation" />
        <Row gutter={16}>
          <Col span={18}>
            <TaskManagement projectId={project.id} />
          </Col>
          <Col span={6}>
            <ProjectSettings projectId={project.id} />
          </Col>
          <Col span={24}>
            <Calculation projectId={project.id} />
          </Col>
        </Row>
      </div>
    ) : null;
  }
}

const Estimation = props => (
  <InnerPageLayoutContainer
    page="estimation"
    companyUri={props.match.params.companyUri}
    title={props.project ? `${props.project.name}:` : ' '}
  >
    <EstimationContent {...props} />
  </InnerPageLayoutContainer>
);


export default Estimation;
