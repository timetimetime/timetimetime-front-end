import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './Estimation';
import { getModels } from '../../actions/models';

const _ = require('lodash');

function mapStateToProps(state, ownProps) {
  return {
    project: _.find(state.projects, { uri: ownProps.match.params.projectUri }),
    companyId: state.current.company,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getModels,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
