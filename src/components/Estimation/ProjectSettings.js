import React, { Component } from 'react';
import { Card, Form, Input, Button, Select, message, Radio, InputNumber } from 'antd';
import { translate } from 'react-i18next';

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class ProjectSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projectSavingInProgress: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(event) {
    event.preventDefault();
    const { t } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const model = Object.assign({}, values, { id: this.props.project.id });
        this.setState({ projectSavingInProgress: true });
        this.props.updateProject(model).then(() => {
          this.setState({ projectSavingInProgress: false });
          message.success(t('projectSavedMessage'));
        }).catch(() => {
          this.setState({ projectSavingInProgress: false });
          message.error(t('form:error.unknown'));
        });
      }
    });
  }
  render() {
    const { project, t } = this.props;
    const { projectSavingInProgress } = this.state;
    const {
      getFieldDecorator,
      getFieldsError,
    } = this.props.form;
    return (
      <div>
        <h3>
          { t('projectSettingsTitle') }
        </h3>
        <Card>
          <Form
            id="project-settings"
            hideRequiredMark
            onSubmit={this.handleSubmit}
          >
            <Form.Item label={t('projectNameLabel')}>
              {
                getFieldDecorator('name', {
                  initialValue: project.name,
                  rules: [
                    { required: true, message: t('form:error.required') },
                    { min: 1, message: t('form:error.tooShort') },
                    { max: 64, message: t('form:error.tooLong') },
                  ],
                })(<Input placeholder={t('projectNamePlaceholder')} />)
              }
            </Form.Item>
            <Form.Item label={t('projectInsuranceLabel')}>
              {
                getFieldDecorator('insurance', {
                  initialValue: project.insurance,
                  rules: [
                    { required: true, message: t('form:error.required') },
                    { type: 'integer', message: t('form:error.isNotInteger') },
                  ],
                })(<InputNumber min={0} max={1000} style={{ width: '100%' }} placeholder={t('projectInsurancePlaceholder')} />)
              }
            </Form.Item>
            <Form.Item label={t('projectIncomeLabel')}>
              {
                getFieldDecorator('income', {
                  initialValue: project.income,
                  rules: [
                    { required: true, message: t('form:error.required') },
                    { type: 'integer', message: t('form:error.isNotInteger') },
                  ],
                })(<InputNumber min={0} max={1000} style={{ width: '100%' }} placeholder={t('projectIncomePlaceholder')} />)
              }
            </Form.Item>
            <Form.Item label={t('projectCommissionLabel')}>
              {
                getFieldDecorator('salesmanCommission', {
                  initialValue: project.salesmanCommission,
                  rules: [
                    { required: true, message: t('form:error.required') },
                    { type: 'integer', message: t('form:error.isNotInteger') },
                  ],
                })(<InputNumber min={0} max={1000} style={{ width: '100%' }} placeholder={t('projectCommissionPlaceholder')} />)
              }
            </Form.Item>
            <Form.Item label={t('salesmanCommissionBaseLabel')}>
              {
                getFieldDecorator('salesmanCommissionBase', {
                  initialValue: project.salesmanCommissionBase,
                  rules: [
                    { required: true, message: t('form:error.required') },
                  ],
                })(<RadioGroup style={{ width: '100%' }}>
                  <RadioButton value="total" style={{ width: '50%', textAlign: 'center' }}>
                    { t('commissionFromTotal') }
                  </RadioButton>
                  <RadioButton value="income" style={{ width: '50%', textAlign: 'center' }}>
                    { t('commissionFromIncome') }
                  </RadioButton>
                </RadioGroup>)
              }
            </Form.Item>
            <Form.Item label={t('projectTaxesLabel')}>
              {
                getFieldDecorator('taxes', {
                  initialValue: project.taxes,
                  rules: [
                    { required: true, message: t('form:error.required') },
                    { type: 'integer', message: t('form:error.isNotInteger') },
                  ],
                })(<InputNumber min={0} max={1000} style={{ width: '100%' }} placeholder={t('projectTaxesPlaceholder')} />)
              }
            </Form.Item>
            <Form.Item label={t('projectTypeLabel')}>
              {
                getFieldDecorator('type', {
                  initialValue: project.type,
                })(
                  <Select
                    style={{ width: '100%' }}
                    placeholder="Type"
                    dropdownClassName="project-type-select"
                  >
                    <Select.Option key="1" value="agile">
                      Agile
                    </Select.Option>
                    <Select.Option key="2" value="waterfall">
                      Waterfall
                    </Select.Option>
                  </Select>
                )
              }
            </Form.Item>
            <Form.Item style={{ margin: 0 }}>
              <Button
                type="primary"
                htmlType="submit"
                disabled={hasErrors(getFieldsError())}
                style={{ width: '100%' }}
                loading={projectSavingInProgress}
              >
                {t('saveProjectSettingsButton')}
              </Button>
            </Form.Item>
          </Form>
        </Card>
      </div>
    );
  }
}

export default translate(['estimation', 'form'])(Form.create()(ProjectSettings));
