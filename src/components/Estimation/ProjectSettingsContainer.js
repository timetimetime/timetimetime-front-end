import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './ProjectSettings';
import { updateModel } from '../../actions/models';

const _ = require('lodash');

function mapStateToProps(state, ownProps) {
  return {
    project: _.find(state.projects, { id: ownProps.projectId }),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    updateProject: model => updateModel('projects', model),
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
