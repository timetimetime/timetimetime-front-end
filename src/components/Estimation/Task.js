import React, { Component } from 'react';
import { Card, Row, Col, Button, Form, Input, Select, Popconfirm, message, Switch } from 'antd';
import { translate } from 'react-i18next';

const _ = require('lodash');

class Task extends Component {
  constructor(props) {
    super(props);
    this.state = {
      savingInProgress: false,
      deletingInProgess: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
  }
  handleSubmit(event) {
    event.preventDefault();
    const { t } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({ savingInProgress: true });
        const task = Object.assign({}, {
          id: this.props.task.id,
          name: values.name,
          isActive: values.isActive,
        });
        const requests = [];
        const taskUpdate = this.props.updateTask(task);
        requests.push(taskUpdate);
        if (values.newSubtasks) {
          const { newSubtasks } = values;
          Object.keys(newSubtasks).forEach((subtaskCategoryId) => {
            const subtask = newSubtasks[subtaskCategoryId];
            if (typeof (subtask.duration) !== 'undefined' && typeof (subtask.performerId) !== 'undefined') {
              const model = Object.assign({}, subtask, {
                subtaskCategoryId,
                taskId: this.props.task.id,
              });
              const subtaskCreation = this.props.createSubtask(model);
              requests.push(subtaskCreation);
            }
          });
        }
        if (values.subtasks) {
          const { subtasks } = values;
          Object.keys(subtasks).forEach((subtaskId) => { // subtask, subtaskId
            const subtask = subtasks[subtaskId];
            if (typeof (subtask.duration) !== 'undefined' && typeof (subtask.performerId) !== 'undefined') {
              const model = Object.assign({}, subtask, {
                id: subtaskId,
              });
              const subtaskUpdate = this.props.updateSubtask(model);
              requests.push(subtaskUpdate);
            }
          });
        }
        Promise.all(requests).then(() => {
          this.setState({ savingInProgress: false });
          message.success(t('taskSavedMessage'));
        }).catch((error) => {
          this.setState({ savingInProgress: false });
          if (error.code === 403 && error.message === 'MODELS_NUMBER_LIMIT') {
            message.warning(t('subtaskNumberLimit'));
            return true;
          }
          message.error(t('form:error.unknown'));
          return error;
        });
      }
    });
  }
  handleDelete() {
    const { deleteTask, task, t } = this.props;
    this.setState({ deletingInProgess: true });
    deleteTask(task.id).then(() => {
      this.setState({ deletingInProgess: false });
      message.success(t('taskDeletedMessage'));
    }).catch(() => {
      this.setState({ deletingInProgess: false });
      message.error(t('form:error.unknown'));
    });
  }
  render() {
    const {
      task,
      subtasks,
      subtaskCategories,
      performers,
      t,
    } = this.props;
    const {
      getFieldDecorator,
    } = this.props.form;
    return (
      <Card
        data-task-name={task.name}
        title={task.name}
        extra={
          <Popconfirm
            title={t('commonui:sureToDeleteMessage')}
            okText={t('commonui:confirm')}
            cancelText={t('commonui:cancel')}
            onConfirm={this.handleDelete}
          >
            <Button
              size="small"
              type="danger"
              htmlType="button"
              ghost
              loading={this.state.deletingInProgess}
            >
              {t('deleteTaskButton')}
            </Button>
          </Popconfirm>
        }
        style={{ marginBottom: '16px' }}
      >
        <Form hideRequiredMark onSubmit={this.handleSubmit}>
          {/*
            <Row gutter={16}>
              <Col span={18}>
                <Form.Item label={t('taskNameLabel')}>
                  {
                    getFieldDecorator('name', {
                      initialValue: task.name,
                      rules: [
                        { required: true, message: 'Enter name please' },
                        { min: 1, message: 'Too short' },
                        { max: 64, message: 'Too long' },
                      ],
                    })(<Input placeholder={t('taskNamePlaceholder')} />)
                  }
                </Form.Item>
              </Col>
              <Col span={6}>
                <Form.Item label={t('taskActivityLabel')}>
                  {getFieldDecorator('isActive', { initialValue: task.isActive })(<Switch defaultChecked={task.isActive} />)}
                </Form.Item>
              </Col>
            </Row>
          */}
          <Row gutter={16}>
            {
              subtaskCategories.map((category) => {
                const existingSubtask = _.find(subtasks, {
                  taskId: task.id,
                  subtaskCategoryId: category.id,
                });
                return (
                  <Col data-category={category.name} key={category.id} span={6}>
                    <Form.Item label={t('subtaskDurationLabel', { activity: category.name })}>
                      {
                        getFieldDecorator(existingSubtask ? `subtasks[${existingSubtask.id}].duration` : `newSubtasks[${category.id}].duration`, {
                          initialValue: existingSubtask ? existingSubtask.duration : undefined,
                          rules: [
                            // { required: true, message: 'Enter something please' },
                            // { min: 1, message: 'Too short' },
                            // { max: 64, message: 'Too long' },
                          ],
                        })(<Input placeholder={t('subtaskDurationPlaceholder')} />)
                      }
                    </Form.Item>
                    <Form.Item label={t('performerLabel')}>
                      {
                        getFieldDecorator(existingSubtask ? `subtasks[${existingSubtask.id}].performerId` : `newSubtasks[${category.id}].performerId`, {
                          initialValue: existingSubtask ? existingSubtask.performerId : performers.filter(el => el.subtaskCategories.includes(category.id)).length === 1 ? performers.filter(el => el.subtaskCategories.includes(category.id))[0].id : undefined,
                        })(
                          <Select
                            dropdownClassName={`${category.name} performer`}
                            style={{ width: '100%' }}
                            placeholder={t('performerPlaceholder')}
                            showSearch
                            filterOption={
                              (input, option) => option.props.children
                                .toLowerCase().includes(input.toLowerCase())
                            }
                          >
                            {
                              performers
                                .filter(el => el.subtaskCategories.includes(category.id))
                                .map(performer => (
                                  <Select.Option
                                    key={performer.id}
                                    value={performer.id}
                                  >
                                    {`${performer.firstName} ${performer.lastName}`}
                                  </Select.Option>
                              ))
                            }
                          </Select>)
                      }
                    </Form.Item>
                  </Col>
                );
              })
            }
          </Row>
          <Row gutter={16}>
            <Col span={24}>
              <Form.Item style={{ marginBottom: 0 }}>
                <Button
                  type="primary"
                  htmlType="submit"
                  disabled={false}
                  style={{ width: '100%' }}
                  loading={this.state.savingInProgress}
                >
                  {t('saveTaskButton')}
                </Button>
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </Card>
    );
  }
}

export default translate(['estimation', 'form', 'commonui'])(Form.create()(Task));
