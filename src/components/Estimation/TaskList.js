import React, { Component } from 'react';
import { translate } from 'react-i18next';
import { Spin } from 'antd';
import Task from './Task';
import StatusWrapper from '../StatusWrapper/StatusWrapper';

class TaskList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      limit: 1,
      isInitialLoadingDone: false,
      loadedTasksNumber: 0,
      totalTasksNumber: 0,
    };
    this.loadTasks = this.loadTasks.bind(this);
  }
  componentDidMount() {
    const {
      getSubtaskCategories,
      companyId,
      getPerformers,
    } = this.props;
    this.loadTasks();
    getSubtaskCategories({ companyId });
    getPerformers({ companyId });
  }
  async loadTasks() {
    const { getTasks, getSubtasks, projectId } = this.props;
    const { limit } = this.state;
    let loadedTasksNumber = 0;
    let totalTasksNumber = 0;
    await getTasks({ projectId, $limit: limit }).then((tasks) => {
      const requests = tasks.data.map(task => getSubtasks({ taskId: task.id }));
      loadedTasksNumber += tasks.data.length;
      totalTasksNumber = tasks.total;
      this.setState({ loadedTasksNumber, totalTasksNumber });
      return Promise.all(requests);
    });
    if (loadedTasksNumber < totalTasksNumber) {
      /* eslint-disable no-await-in-loop, no-loop-func */
      while (loadedTasksNumber < totalTasksNumber) {
        await getTasks({ projectId, $limit: limit, $skip: loadedTasksNumber }).then((tasks) => {
          const requests = tasks.data.map(task => getSubtasks({ taskId: task.id }));
          loadedTasksNumber += tasks.data.length;
          totalTasksNumber = tasks.total;
          this.setState({ loadedTasksNumber, totalTasksNumber });
          return Promise.all(requests);
        });
      }
      /* eslint-enable no-await-in-loop, no-loop-func */
      this.setState({ isInitialLoadingDone: true });
    } else {
      this.setState({ isInitialLoadingDone: true });
    }
  }
  render() {
    const { tasks, t } = this.props;
    const { isInitialLoadingDone, loadedTasksNumber, totalTasksNumber } = this.state;
    return (
      <div>
        {
          isInitialLoadingDone && tasks.length > 0 ?
          tasks.map(task => (
            <Task
              key={task.id}
              task={task}
              {...this.props}
            />
          ))
          :
          <StatusWrapper height="656px">
            {
              isInitialLoadingDone ?
                <p>{t('noTasks')}</p>
              :
                <div style={{ padding: '6px' }}>
                  <Spin />
                  {totalTasksNumber !== 0 ? <p style={{ padding: '6px 0', fontSize: '12px' }}>{`${loadedTasksNumber}/${totalTasksNumber}`}</p> : null}
                </div>
            }
          </StatusWrapper>
        }
      </div>
    );
  }
}

export default translate(['estimation'])(TaskList);
