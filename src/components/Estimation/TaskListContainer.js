import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './TaskList';
import { getModels, updateModel, deleteModel, createModel } from '../../actions/models';

const _ = require('lodash');

function getElementNesting(el, collection, attribute, level = -1) {
  if (typeof el.level === 'undefined') {
    el.level = level + 1;
  } else {
    el.level = el.level < level ? level : el.level;
  }
  const descendants = collection.filter(e => e[attribute].includes(el.id));
  descendants.forEach(desc => getElementNesting(desc, collection, attribute, el.level));
}

function setNestingLevel(collection, attribute) {
  const beginning = collection.filter(el => el[attribute].length === 0);
  beginning.forEach((el) => {
    getElementNesting(el, collection, attribute);
  });
}

function mapStateToProps(state, ownProps) {
  const { projectId } = ownProps;
  // Get subtaskCategories
  let subtaskCategories = [];
  const project = state.projects.filter(el => el.id === projectId)[0];
  if (project) {
    subtaskCategories = state.subtaskCategories
      .filter(subtaskCategory => subtaskCategory.serviceId === project.serviceId);
  }
  setNestingLevel(subtaskCategories, 'predecessors');
  // Get tasks
  const tasks = state.tasks.filter(task => task.projectId === projectId);
  // Get subtasks
  const taskIds = tasks.map(task => task.id);
  const subtasks = state.subtasks.filter(subtask => taskIds.includes(subtask.taskId));
  return {
    tasks: _.orderBy(tasks, ['createdAt'], ['desc']),
    subtasks,
    subtaskCategories: _.orderBy(subtaskCategories, ['level'], ['asc']),
    performers: state.performers,
    companyId: state.current.company,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getTasks: params => getModels('tasks', params),
    updateTask: model => updateModel('tasks', model),
    deleteTask: id => deleteModel('tasks', id),
    getSubtaskCategories: params => getModels('subtaskCategories', params),
    getPerformers: params => getModels('performers', params),
    getSubtasks: params => getModels('subtasks', params),
    updateSubtask: model => updateModel('subtasks', model),
    createSubtask: model => createModel('subtasks', model),
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
