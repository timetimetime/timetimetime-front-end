import React, { Component } from 'react';
import { Button } from 'antd';
import { translate } from 'react-i18next';
import CreateTask from './CreateTaskContainer';
import TaskList from './TaskListContainer';

class TaskManagement extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isTaskCreationModalShown: false,
    };
    this.showTaskCreationModal = this.showTaskCreationModal.bind(this);
    this.hideTaskCreationModal = this.hideTaskCreationModal.bind(this);
  }
  showTaskCreationModal() {
    this.setState({
      isTaskCreationModalShown: true,
    });
  }
  hideTaskCreationModal() {
    this.setState({
      isTaskCreationModalShown: false,
    });
  }
  render() {
    const { t } = this.props;
    const { isTaskCreationModalShown } = this.state;
    return (
      <div>
        <h3>
          {t('tasksManagementTitle')}
          <Button
            id="add-tasks-btn"
            type="primary"
            size="small"
            onClick={this.showTaskCreationModal}
            style={{ float: 'right' }}
          >
            {t('addTasksButton')}
          </Button>
        </h3>
        <CreateTask
          isShown={isTaskCreationModalShown}
          onCancel={this.hideTaskCreationModal}
          projectId={this.props.projectId}
        />
        <TaskList {...this.props} />
      </div>
    );
  }
}

export default translate(['estimation', 'form'])(TaskManagement);
