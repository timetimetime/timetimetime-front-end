import React, { Component } from 'react';
import {
  Form,
  Button,
  Input,
  InputNumber,
  message,
} from 'antd';
import { translate } from 'react-i18next';

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class CreateExpense extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(event) {
    event.preventDefault();
    const { t } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const model = Object.assign({}, values, { companyId: this.props.companyId });
        this.props.createExpense(model).then(() => {
          message.success(t('expenseCreatedMessage'));
          this.props.form.resetFields();
        }).catch((error) => {
          if (error.code === 403 && error.message === 'MODELS_NUMBER_LIMIT') {
            message.warning(t('modelsNumberLimit'));
            return true;
          }
          message.error(t('form:error.unknown'));
          return error;
        });
      }
    });
  }
  render() {
    const { t } = this.props;
    const {
      getFieldDecorator,
      getFieldsError,
    } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark>
        <Form.Item label={t('expenseNameLabel')}>
          {
            getFieldDecorator('name', {
              rules: [
                { required: true, message: t('form:error.required') },
                { min: 1, message: t('form:error.tooShort') },
                { max: 64, message: t('form:error.tooLong') },
              ],
            })(<Input placeholder={t('expenseNamePlaceholder')} />)
          }
        </Form.Item>
        <Form.Item label={t('expenseCostLabel')}>
          {
            getFieldDecorator('cost', {
              rules: [
                { required: true, message: t('form:error.required') },
                { type: 'integer', message: t('form:error.isNotInteger') },
              ],
            })(<InputNumber min={0} max={2147483647} style={{ width: '100%' }} placeholder={t('expenseCostPlaceholder')} />)
          }
        </Form.Item>
        <Form.Item style={{ margin: 0 }}>
          <Button
            type="primary"
            htmlType="submit"
            disabled={hasErrors(getFieldsError())}
            style={{ width: '100%' }}
          >
            {t('createExpenseSubmit')}
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default translate(['expenses', 'form'])(Form.create()(CreateExpense));
