import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './CreateExpense';
import { createModel } from '../../actions/models';

function mapStateToProps(state) {
  return {
    companyId: state.current.company,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createExpense: model => createModel('expenses', model),
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
