import React from 'react';
import { translate } from 'react-i18next';
import { Row, Col, Card } from 'antd';
import Tutorial from '../Tutorial/TutorialContainer';
import CreateExpense from './CreateExpenseContainer';
import ExpensesList from './ExpensesListContainer';
import InnerPageLayoutContainer from '../InnerPageLayout/InnerPageLayoutContainer';

const Expenses = ({ t, match }) => (
  <InnerPageLayoutContainer
    page="expenses"
    companyUri={match.params.companyUri}
    title={t('title')}
  >
    <Tutorial theme="expenses" />
    <Row gutter={16}>
      <Col span={18}>
        <ExpensesList />
      </Col>
      <Col span={6}>
        <Card title={t('addExpenseTitle')}>
          <CreateExpense />
        </Card>
      </Col>
    </Row>
  </InnerPageLayoutContainer>
);

export default translate(['expenses'])(Expenses);
