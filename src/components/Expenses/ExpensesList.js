import React from 'react';
import { translate } from 'react-i18next';
import DataGrid from '../DataGrid/DataGrid';

function getColumns(t) {
  return [
    {
      title: t('expenseNameColumn'),
      dataIndex: 'name',
      width: '50%',
    },
    {
      title: t('expenseCostColumn'),
      dataIndex: 'cost',
      width: '25%',
      number: {
        min: 0,
        max: 2147483647,
        placeholder: t('costPlaceholder'),
      },
    },
  ];
}

function getTextKeys(t) {
  return {
    emptyText: t(),
  };
}

const modelName = 'expenses';

const ExpensesList = ({
  t,
  expenses,
  getModels,
  companyId,
  deleteModel,
  updateModel,
}) => (
  <DataGrid
    columns={getColumns(t)}
    textKeys={getTextKeys(t)}
    records={expenses}
    getRecords={params => getModels(modelName, Object.assign({}, params, { companyId }))}
    deleteRecord={params => deleteModel(modelName, params)}
    updateRecord={params => updateModel(modelName, params)}
  />
);

export default translate(['expenses', 'form', 'commonui'])(ExpensesList);
