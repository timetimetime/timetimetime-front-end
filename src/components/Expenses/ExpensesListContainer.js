import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './ExpensesList';
import { getModels, updateModel, deleteModel } from '../../actions/models';

function mapStateToProps(state) {
  const companyId = state.current.company;
  return {
    expenses: state.expenses.filter(expense => expense.companyId === companyId),
    companyId,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getModels,
    updateModel,
    deleteModel,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
