import React, { Component } from 'react';
import { Layout, message } from 'antd';
import { Redirect } from 'react-router-dom';
import { translate } from 'react-i18next';

import NavigationContainer from '../Navigation/NavigationContainer';
import Sorry from '../Sorry/Sorry';

const jwtDecode = require('jwt-decode');

const { Header, Footer, Content } = Layout;

const Page = ({ title, children, initialized, companyUri }) => (
  <Layout>
    <Header>
      <div className="container">
        <NavigationContainer companyUri={companyUri} />
      </div>
    </Header>
    <Content>
      <div className="container" style={{ marginBottom: '32px' }}>
        { title ? <h1 style={{ marginTop: '.5em' }}>{title}</h1> : null }
        { initialized ? children : null }
      </div>
    </Content>
    <Footer style={{
      backgroundColor: '#ddd',
      padding: '8px 50px',
      fontSize: '12px',
      textAlign: 'right',
    }}
    >
      <div className="container">
        tempus edax rerum
      </div>
    </Footer>
    <Sorry />
  </Layout>
);

class InnerPageLayout extends Component {
  constructor(props) {
    super(props);
    this.state = { initialized: false };
  }
  componentDidMount() {
    const {
      getCompanies,
      companyUri,
      setCurrentUser,
      setCurrentCompany,
      getModel,
      logger,
      page,
      t,
    } = this.props;
    window.onbeforeunload = () => {
      logger({
        action: 'close_app',
        type: 'navigation',
        payload: { page },
      });
    };
    const accessToken = localStorage.getItem('accessToken');
    if (accessToken) {
      const requests = [];
      const { userId } = jwtDecode(accessToken);
      setCurrentUser(userId);
      logger({
        action: 'go_to',
        type: 'navigation',
        payload: { page },
      });
      requests.push(getModel('users', userId));
      const companyRequest = getCompanies({ uri: companyUri }).then((response) => {
        const companies = response.data;
        if (companies.length) {
          const currentCompanyId = companies[0].id;
          setCurrentCompany(currentCompanyId);
        } else {
          alert('???');
        }
      });
      requests.push(companyRequest);
      Promise.all(requests).then((data) => {
        this.setState({ initialized: true });
        if (this.props.requestEmailConfirmation === true) {
          const user = data[0];
          const createdAt = new Date(user.createdAt);
          const isNotNewUser = Date.now() - createdAt.getTime() > 900000; // 15 minutes
          if (user.isEmailConfirmed === false && isNotNewUser) {
            message.warning(t('emailIsNotConfirmed'));
            this.props.setEmailConfirmationRequesting(false);
          }
        }
      }).catch((err) => { throw err; });
    }
  }
  render() {
    const hasAccessToken = !!localStorage.getItem('accessToken');
    return hasAccessToken ? <Page {...this.props} initialized={this.state.initialized} /> : <Redirect push to={`/sign-in?from=${window.location.pathname}`} />;
  }
}

export default translate(['various'])(InnerPageLayout);
