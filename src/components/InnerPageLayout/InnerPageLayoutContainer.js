import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './InnerPageLayout';
import { getModel, getModels } from '../../actions/models';
import { setCurrentUser, setCurrentCompany, setEmailConfirmationRequesting } from '../../actions/current';
import logger from '../../utils/logger';

function mapStateToProps(state) {
  return {
    requestEmailConfirmation: state.current.requestEmailConfirmation,
  };
}

function mapDispatchToProps(dispatch) {
  const actions = bindActionCreators({
    getCompanies: params => getModels('companies', params),
    setCurrentUser,
    setCurrentCompany,
    getModel,
    setEmailConfirmationRequesting,
  }, dispatch);
  return Object.assign(actions, { logger });
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
