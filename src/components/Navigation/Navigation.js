import React, { Component } from 'react';
import { Menu } from 'antd';
import { Redirect } from 'react-router-dom';
import { translate } from 'react-i18next';
import server from '../../utils/server';

class Navigation extends Component {
  constructor(props) {
    super(props);
    this.state = { goTo: null };
    this.handleClick = this.handleClick.bind(this);
  }
  handleClick(event) {
    const { key } = event;
    if (key === 'logout') {
      this.props.resetState();
      server.logout();
      this.setState({ goTo: '/sign-in' });
    } else {
      this.setState({ goTo: key });
    }
  }
  render() {
    const { goTo } = this.state;
    const { companyUri, t } = this.props;
    return (
      <div>
        { goTo ? <Redirect push to={goTo} /> : null }
        <Menu
          mode="horizontal"
          theme="dark"
          onClick={this.handleClick}
          style={{ lineHeight: '64px' }}
        >
          <Menu.Item key={`/${companyUri}`}>
            {t('projects')}
          </Menu.Item>
          <Menu.SubMenu title={<span>{t('dataForCalculations')}</span>}>
            <Menu.Item key={`/${companyUri}/product-types`}>
              {t('services')}
            </Menu.Item>
            <Menu.Item key={`/${companyUri}/work-categories`}>
              {t('subtaskCategories')}
            </Menu.Item>
            <Menu.Item key={`/${companyUri}/performers`}>
              {t('performers')}
            </Menu.Item>
            <Menu.Item key={`/${companyUri}/expenses`}>
              {t('indirectExpenses')}
            </Menu.Item>
          </Menu.SubMenu>
          <Menu.Item key={`/${companyUri}/subscription`}>
            {t('subscription')}
          </Menu.Item>
          <Menu.SubMenu title={<span>{t('personalSettings')}</span>}>
            <Menu.Item key={`/${companyUri}/profile`}>
              {t('userSettings')}
            </Menu.Item>
            <Menu.Item key="/enter">
              {t('changeCompany')}
            </Menu.Item>
            <Menu.Item key="logout">
              {t('logout')}
            </Menu.Item>
          </Menu.SubMenu>
        </Menu>
      </div>
    );
  }
}

export default translate(['navigation'])(Navigation);
