import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Navigation from './Navigation';
import { getUser } from '../../actions/users';
import { resetState } from '../../actions/various';

function mapStateToProps(state) {
  return {
    users: state.users,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getUser,
    resetState,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
