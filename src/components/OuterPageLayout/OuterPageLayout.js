import React, { Component } from 'react';
import './OuterPageLayout.css';
import Sorry from '../Sorry/Sorry';

export default class OuterPageLayout extends Component {
  componentDidMount() {
    const { logger, page } = this.props;
    window.onbeforeunload = () => {
      logger({
        action: 'close_app',
        type: 'navigation',
        payload: { page },
      });
    };
    logger({
      action: 'go_to',
      type: 'navigation',
      payload: { page },
    });
  }
  render() {
    const { children } = this.props;
    return (
      <div className="outer-page-layout">
        { children }
        <Sorry />
      </div>
    );
  }
}
