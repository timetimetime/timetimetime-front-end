import { connect } from 'react-redux';
// import { bindActionCreators } from 'redux';
import Component from './OuterPageLayout';
import logger from '../../utils/logger';

function mapStateToProps() {
  return {};
}

function mapDispatchToProps() {
  return { logger };
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
