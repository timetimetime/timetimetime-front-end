import React, { Component } from 'react';
import { Redirect, Link } from 'react-router-dom';
import {
  Form,
  Button,
  Input,
  message,
  Row,
  Col,
  Card,
} from 'antd';
import { translate } from 'react-i18next';
import OuterPageLayout from '../OuterPageLayout/OuterPageLayoutContainer';

const toSignUpStyles = { height: '33px', lineHeight: '33px' };

class PasswordRecovery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      success: false,
      isWaitingForServerResponse: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(event) {
    event.preventDefault();
    const { t } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({ isWaitingForServerResponse: true });
        this.props.recoverPassword(values.email).then(() => {
          message.success(t('passwordRecoveryStarted'), 5);
          this.setState({
            success: true,
            isWaitingForServerResponse: false,
          });
        }).catch((error) => {
          this.setState({ isWaitingForServerResponse: false });
          if (error.code === 400) {
            if (error.message === 'USER_DOES_NOT_EXIST') {
              message.warning(t('userNotFound'));
              return true;
            } else if (error.message === 'USER_EMAIL_IS_NOT_CONFIRMED') {
              message.warning(t('emailIsNotConfirmed'));
              return true;
            }
          }
          message.error(t('form:error.unknown'));
        });
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { t } = this.props;
    const { isWaitingForServerResponse } = this.state;
    return (
      <OuterPageLayout page="password-recovery">
        <div className="container">
          <Row>
            <Col span={6} offset={9}>
              <Card title={t('title')}>
                <Form
                  layout="vertical"
                  style={{ width: '100%' }}
                  onSubmit={this.handleSubmit}
                >
                  <Form.Item>
                    {
                      getFieldDecorator('email', {
                        validateTrigger: 'onBlur',
                        rules: [
                          { required: true, message: t('form:error.required') },
                          { type: 'email', message: t('form:error.emailFormat') },
                        ],
                      })(<Input autoFocus placeholder={t('email')} />)
                    }
                  </Form.Item>
                  <Form.Item style={{ margin: 0 }}>
                    <Button
                      type="primary"
                      htmlType="submit"
                      style={{ width: '100%' }}
                      loading={isWaitingForServerResponse}
                    >
                      {t('submit')}
                    </Button>
                  </Form.Item>
                </Form>
              </Card>
            </Col>
          </Row>
        </div>
      </OuterPageLayout>
    );
  }
}

export default translate(['passwordRecovery', 'form'])(Form.create()(PasswordRecovery));
