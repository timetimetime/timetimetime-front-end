import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './PasswordRecovery';
import { recoverPassword } from '../../actions/users';

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    recoverPassword,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
