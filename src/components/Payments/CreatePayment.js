import React, { Component } from 'react';
import {
  Form,
  Button,
  message,
  Select,
  InputNumber,
} from 'antd';
import { translate } from 'react-i18next';

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class CreatePayment extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    this.props.getTariffs();
  }
  handleSubmit(event) {
    event.preventDefault();
    const { t, companyId } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const model = Object.assign({}, values, { companyId });
        this.props.createPayment(model).then((payment) => {
          window.location.href = payment.url;
        }).catch((error) => {
          if (error.code === 403 && error.message === 'MODELS_NUMBER_LIMIT') {
            message.warning(t('modelsNumberLimit'));
            return true;
          }
          message.error(t('form:error.unknown'));
          return error;
        });
      }
    });
  }
  render() {
    const { t, tariffs } = this.props;
    const {
      getFieldDecorator,
      getFieldsError,
    } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark>
        <Form.Item label={t('tariffLabel')}>
          {
            getFieldDecorator('tariffId', {
              defaultActiveFirstOption: true,
              rules: [
                { required: true, message: t('form:error.required') },
              ],
            })(
              <Select
                style={{ width: '100%' }}
                placeholder={t('tariffPlaceholder')}
              >
                {
                  tariffs.map(el => (
                    <Select.Option
                      key={el.id}
                      value={el.id}
                    >
                      {`${el.name} (${el.rub} руб.)`}
                    </Select.Option>
                  ))
                }
              </Select>
            )
          }
        </Form.Item>
        <Form.Item label={t('monthsNumberLabel')}>
          {
            getFieldDecorator('monthsNumber', {
              initialValue: 1,
              rules: [
                { required: true, message: t('form:error.required') },
                { type: 'integer', message: t('form:error.isNotInteger') },
              ],
            })(<InputNumber min={0} max={24} style={{ width: '100%' }} placeholder={t('monthsNumberPlaceholder')} />)
          }
        </Form.Item>
        <Form.Item style={{ margin: 0 }}>
          <Button
            type="primary"
            htmlType="submit"
            disabled={hasErrors(getFieldsError())}
            style={{ width: '100%' }}
          >
            {t('createPaymentSubmit')}
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default translate(['payments', 'form'])(Form.create()(CreatePayment));
