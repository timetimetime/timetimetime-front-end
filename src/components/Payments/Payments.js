import React from 'react';
import { translate } from 'react-i18next';
import { Row, Col, Card } from 'antd';
import InnerPageLayoutContainer from '../InnerPageLayout/InnerPageLayoutContainer';
import CreatePayment from './CreatePayment';
import PaymentsList from './PaymentsList';

const Payments = ({
  t,
  match,
  tariffs,
  getTariffs,
  companyId,
  createPayment,
  payments,
  getPayments,
}) => (
  <InnerPageLayoutContainer
    companyUri={match.params.companyUri}
    title={t('pageTitle')}
  >
    <Row gutter={16}>
      <Col span={18}>
        <PaymentsList
          payments={payments}
          getPayments={getPayments}
          companyId={companyId}
          tariffs={tariffs}
        />
      </Col>
      <Col span={6}>
        <Card title={t('createPaymentTitle')}>
          <CreatePayment
            tariffs={tariffs}
            getTariffs={getTariffs}
            companyId={companyId}
            createPayment={createPayment}
          />
        </Card>
      </Col>
    </Row>
  </InnerPageLayoutContainer>
);

export default translate(['payments'])(Payments);
