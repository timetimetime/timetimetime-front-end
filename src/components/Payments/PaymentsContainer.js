import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './Payments';
import { getModels, createModel } from '../../actions/models';

const _ = require('lodash');

function mapStateToProps(state) {
  const companyId = state.current.company;
  return {
    companyId,
    payments: state.payments.filter(el => el.companyId === companyId),
    tariffs: _.sortBy(state.tariffs, [el => parseFloat(el.rub)]),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getTariffs: () => getModels('tariffs', {}),
    getPayments: params => getModels('payments', params),
    createPayment: model => createModel('payments', model),
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
