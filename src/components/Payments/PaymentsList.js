import React from 'react';
import { translate } from 'react-i18next';
import DataGrid from '../DataGrid/DataGrid';

const moment = require('moment');

function getColumns(t, tariffs) {
  return [
    {
      title: t('tariffColumn'),
      dataIndex: 'tariffId',
      width: '25%',
      render: (text, record) => <span>{tariffs.filter(el => el.id === record.tariffId)[0].name}</span>,
    },
    {
      title: t('monthsNumberColumn'),
      dataIndex: 'monthsNumber',
      width: '25%',
    },
    {
      title: t('isPaidColumn'),
      dataIndex: 'isPaid',
      width: '25%',
      render: (text, record) => <span>{record.isPaid ? t('isPaid') : t('isNotPaid')}</span>,
    },
    {
      title: t('createdAtColumn'),
      dataIndex: 'createdAt',
      width: '25%',
      render: (text, record) => <span>{moment(record.createdAt).format('L')}</span>,
    },
  ];
}

function getTextKeys(t) {
  return {
    emptyText: t(),
  };
}

class PaymentsList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isDataLoaded: false,
    };
  }
  componentDidMount() {
    this.props.getPayments({
      companyId: this.props.companyId,
    }).then(() => {
      this.setState({ isDataLoaded: true });
    });
  }
  render() {
    const {
      t,
      payments,
      getPayments,
      companyId,
      tariffs,
    } = this.props;
    const { isDataLoaded } = this.state;
    return isDataLoaded ?
      <DataGrid
        columns={getColumns(t, tariffs)}
        textKeys={getTextKeys(t)}
        records={payments}
        getRecords={params => getPayments(Object.assign({}, params, { companyId }))}
        actionsColumn={false}
      />
      : null;
  }
}

export default translate(['payments', 'form', 'commonui'])(PaymentsList);
