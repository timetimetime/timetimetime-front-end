import React, { Component } from 'react';
import {
  Form,
  Button,
  Input,
  message,
  Select,
  InputNumber,
} from 'antd';
import { translate } from 'react-i18next';

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class CreatePerformer extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    this.props.getSubtaskCategories({
      companyId: this.props.companyId,
    });
  }
  handleSubmit(event) {
    event.preventDefault();
    const { t } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const model = Object.assign({}, values, { companyId: this.props.companyId });
        this.props.createPerformer(model).then(() => {
          message.success(t('performerCreatedMessage'));
          this.props.form.resetFields();
        }).catch((error) => {
          if (error.code === 403 && error.message === 'MODELS_NUMBER_LIMIT') {
            message.warning(t('modelsNumberLimit'));
            return true;
          }
          message.error(t('form:error.unknown'));
          return error;
        });
      }
    });
  }
  render() {
    const { t, subtaskCategories } = this.props;
    const {
      getFieldDecorator,
      getFieldsError,
    } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark>
        <Form.Item label={t('firstNameLabel')}>
          {
            getFieldDecorator('firstName', {
              rules: [
                { required: true, message: t('form:error.required') },
                { min: 1, message: t('form:error.tooShort') },
                { max: 64, message: t('form:error.tooLong') },
              ],
            })(<Input placeholder={t('firstNamePlaceholder')} />)
          }
        </Form.Item>
        <Form.Item label={t('lastNameLabel')}>
          {
            getFieldDecorator('lastName', {
              rules: [
                { required: true, message: t('form:error.required') },
                { min: 1, message: t('form:error.tooShort') },
                { max: 64, message: t('form:error.tooLong') },
              ],
            })(<Input placeholder={t('lastNamePlaceholder')} />)
          }
        </Form.Item>
        <Form.Item label={t('subtaskCategoriesLabel')}>
          {
            getFieldDecorator('subtaskCategories')(
              <Select
                style={{ width: '100%' }}
                placeholder={t('subtaskCategoriesPlaceholder')}
                mode="multiple"
                dropdownClassName="position-select"
              >
                {
                  subtaskCategories.reverse().map(subtaskCategory =>
                    <Select.Option
                      key={subtaskCategory.id}
                      value={subtaskCategory.id}
                    >
                      {subtaskCategory.name}
                    </Select.Option>
                  )
                }
              </Select>
            )
          }
        </Form.Item>
        <Form.Item label={t('salaryLabel')}>
          {
            getFieldDecorator('salary', {
              rules: [
                { required: true, message: t('form:error.required') },
                { type: 'integer', message: t('form:error.isNotInteger') },
              ],
            })(<InputNumber min={0} max={2147483647} style={{ width: '100%' }} placeholder={t('salaryPlaceholder')} />)
          }
        </Form.Item>
        <Form.Item style={{ margin: 0 }}>
          <Button
            type="primary"
            htmlType="submit"
            disabled={hasErrors(getFieldsError())}
            style={{ width: '100%' }}
          >
            {t('createPerformerSubmit')}
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default translate(['performers', 'form'])(Form.create()(CreatePerformer));
