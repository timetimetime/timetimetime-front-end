import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './CreatePerformer';
import { createModel, getModels } from '../../actions/models';

function mapStateToProps(state) {
  const companyId = state.current.company;
  return {
    companyId,
    subtaskCategories: state.subtaskCategories.filter(el => el.companyId === companyId),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createPerformer: model => createModel('performers', model),
    getSubtaskCategories: data => getModels('subtaskCategories', data),
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
