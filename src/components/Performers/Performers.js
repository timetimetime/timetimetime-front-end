import React from 'react';
import { translate } from 'react-i18next';
import { Row, Col, Card } from 'antd';
import Tutorial from '../Tutorial/TutorialContainer';
import CreatePerformer from './CreatePerformerContainer';
import PerformersList from './PerformersListContainer';
import InnerPageLayoutContainer from '../InnerPageLayout/InnerPageLayoutContainer';

const Performers = ({ t, match }) => (
  <InnerPageLayoutContainer
    page="performers"
    companyUri={match.params.companyUri}
    title={t('title')}
  >
    <Tutorial theme="performers" />
    <Row gutter={16}>
      <Col span={18}>
        <PerformersList />
      </Col>
      <Col span={6}>
        <Card title={t('addExpenseTitle')}>
          <CreatePerformer />
        </Card>
      </Col>
    </Row>
  </InnerPageLayoutContainer>
);

export default translate(['performers'])(Performers);
