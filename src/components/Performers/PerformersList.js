import React from 'react';
import { translate } from 'react-i18next';
import DataGrid from '../DataGrid/DataGrid';

function getColumns(t, subtaskCategories) {
  return [
    {
      title: t('performerFirstNameColumn'),
      dataIndex: 'firstName',
      width: '18.75%',
    },
    {
      title: t('performerLastNameColumn'),
      dataIndex: 'lastName',
      width: '18.75%',
    },
    {
      title: t('performerSalaryColumn'),
      dataIndex: 'salary',
      width: '18.75%',
      number: {
        min: 0,
        max: 2147483647,
        placeholder: t('salaryPlaceholder'),
      },
    },
    {
      title: t('subtaskCategoriesLabel'),
      dataIndex: 'subtaskCategories',
      width: '18.75%',
      select: {
        options: subtaskCategories,
        mode: 'multiple',
        placeholder: t('subtaskCategoriesPlaceholder'),
      },
    },
  ];
}

function getTextKeys(t) {
  return {
    emptyText: t(),
  };
}

class PerformersList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isDataLoaded: false,
    };
  }
  componentDidMount() {
    this.props.getSubtaskCategories({
      companyId: this.props.companyId,
    }).then(() => {
      this.setState({ isDataLoaded: true });
    });
  }
  render() {
    const {
      t,
      performers,
      subtaskCategories,
      getPerformers,
      companyId,
      deletePerformer,
      updatePerformer,
    } = this.props;
    const { isDataLoaded } = this.state;
    return isDataLoaded ?
      <DataGrid
        columns={getColumns(t, subtaskCategories)}
        textKeys={getTextKeys(t)}
        records={performers}
        getRecords={params => getPerformers(Object.assign({}, params, { companyId }))}
        deleteRecord={deletePerformer}
        updateRecord={updatePerformer}
      />
      : null;
  }
}

export default translate(['performers', 'form', 'commonui'])(PerformersList);
