import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './PerformersList';
import { getModels, updateModel, deleteModel } from '../../actions/models';

function mapStateToProps(state) {
  const companyId = state.current.company;
  return {
    performers: state.performers.filter(performer => performer.companyId === companyId),
    subtaskCategories: state.subtaskCategories.filter(el => el.companyId === companyId),
    companyId,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getPerformers: params => getModels('performers', params),
    getSubtaskCategories: params => getModels('subtaskCategories', params),
    updatePerformer: model => updateModel('performers', model),
    deletePerformer: id => deleteModel('performers', id),
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
