import React, { Component } from 'react';
import {
  Form,
  Button,
  Input,
  message,
} from 'antd';
import { translate } from 'react-i18next';

class ProfileEditing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isWaitingForServerResponse: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(event) {
    event.preventDefault();
    const { t, user } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const model = Object.assign({}, values, { id: user.id });
        this.setState({ isWaitingForServerResponse: true });
        this.props.updateModel('users', model).then(() => {
          message.success(t('passwordChangedMessage'));
          this.setState({ isWaitingForServerResponse: false });
          this.props.form.resetFields();
        }).catch((error) => {
          console.error(error);
          message.error(t('form:error.unknown'));
          this.setState({ isWaitingForServerResponse: false });
        });
      }
    });
  }
  render() {
    const { isWaitingForServerResponse } = this.state;
    const { t } = this.props;
    const {
      getFieldDecorator,
    } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Item label={t('currentPasswordLabel')}>
          {
            getFieldDecorator('password', {
              rules: [
                { required: true, message: t('form:error.required') },
                { min: 1, message: t('form:error.tooShort') },
                { max: 48, message: t('form:error.tooLong') },
              ],
            })(<Input type="password" placeholder={t('currentPasswordPlaceholder')} />)
          }
        </Form.Item>
        <Form.Item label={t('newPasswordLabel')}>
          {
            getFieldDecorator('newPassword', {
              rules: [
                { required: true, message: t('form:error.required') },
                { min: 7, message: t('minPasswordLength') },
                { max: 48, message: t('form:error.tooLong') },
              ],
            })(<Input type="password" placeholder={t('newPasswordPlaceholder')} />)
          }
        </Form.Item>
        <Form.Item style={{ margin: 0 }}>
          <Button
            type="primary"
            htmlType="submit"
            style={{ width: '100%' }}
            loading={isWaitingForServerResponse}
          >
            {t('changePasswordSubmit')}
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default translate(['personalProfile', 'form'])(Form.create()(ProfileEditing));
