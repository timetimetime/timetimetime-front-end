import React from 'react';
import { translate } from 'react-i18next';
import { Row, Col, Card } from 'antd';
import InnerPageLayoutContainer from '../InnerPageLayout/InnerPageLayoutContainer';
import ProfileEditing from './ProfileEditing';
import PasswordChanging from './PasswordChanging';
import ProjectDefaults from './ProjectDefaults';

const Projects = ({
  t,
  match,
  user,
  companyId,
  updateModel,
  getModels,
  projectDefaults,
}) => (
  <InnerPageLayoutContainer
    page="profile"
    companyUri={match.params.companyUri}
    title={t('title')}
  >
    <Row gutter={16}>
      <Col span={8}>
        <Card title={t('personalSettingTitle')}>
          <ProfileEditing user={user} updateModel={updateModel} />
        </Card>
      </Col>
      <Col span={8}>
        <Card title={t('passwordChangeTitle')}>
          <PasswordChanging user={user} updateModel={updateModel} />
        </Card>
      </Col>
      <Col span={8}>
        <Card title={t('projectDefaultsTitle')}>
          <ProjectDefaults
            companyId={companyId}
            getModels={getModels}
            updateModel={updateModel}
            projectDefaults={projectDefaults}
          />
        </Card>
      </Col>
    </Row>
  </InnerPageLayoutContainer>
);

export default translate(['personalProfile'])(Projects);
