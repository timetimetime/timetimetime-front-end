import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './PersonalProfile';
import { updateModel, getModels } from '../../actions/models';

function mapStateToProps(state) {
  const companyId = state.current.company;
  return {
    user: state.users.filter(el => el.id === state.current.user)[0],
    companyId,
    projectDefaults: state.projectDefaults.filter(el => el.companyId === companyId)[0],
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    updateModel,
    getModels,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
