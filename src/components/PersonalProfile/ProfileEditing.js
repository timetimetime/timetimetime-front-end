import React, { Component } from 'react';
import {
  Form,
  Button,
  Input,
  message,
  Row,
  Col,
  Switch,
  Card,
} from 'antd';
import { translate } from 'react-i18next';

class ProfileEditing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isWaitingForServerResponse: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(event) {
    event.preventDefault();
    const { t, user } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const model = Object.assign({}, values, { id: user.id });
        const hasEmailChange = !(model.email === user.email);
        if (!hasEmailChange) {
          delete model.email;
        }
        this.setState({ isWaitingForServerResponse: true });
        this.props.updateModel('users', model).then(() => {
          message.success(t('profileSavedMessage'));
          if (hasEmailChange) {
            message.warning(t('emailChangeNotification'), 5);
          }
          this.setState({ isWaitingForServerResponse: false });
        }).catch((error) => {
          console.error(error);
          message.error(t('form:error.unknown'));
          this.setState({ isWaitingForServerResponse: false });
        });
      }
    });
  }
  render() {
    const { isWaitingForServerResponse } = this.state;
    const { t, user } = this.props;
    const {
      getFieldDecorator,
    } = this.props.form;
    return (
      <Form onSubmit={this.handleSubmit}>
        <Form.Item label={t('firstNameLabel')}>
          {
            getFieldDecorator('firstName', {
              initialValue: user.firstName,
              rules: [
                // { required: true, message: t('form:error.required') },
                { min: 1, message: t('form:error.tooShort') },
                { max: 64, message: t('form:error.tooLong') },
              ],
            })(<Input placeholder={t('firstNamePlaceholder')} />)
          }
        </Form.Item>
        <Form.Item label={t('lastNameLabel')}>
          {
            getFieldDecorator('lastName', {
              initialValue: user.lastName,
              rules: [
                // { required: true, message: t('form:error.required') },
                { min: 1, message: t('form:error.tooShort') },
                { max: 64, message: t('form:error.tooLong') },
              ],
            })(<Input placeholder={t('lastNamePlaceholder')} />)
          }
        </Form.Item>
        <Form.Item label={t('emailLabel')}>
          {
            getFieldDecorator('email', {
              initialValue: user.email,
              rules: [
                { required: true, message: t('form:error.required') },
                { type: 'email', message: t('form:error.emailFormat') },
                { min: 1, message: t('form:error.tooShort') },
                { max: 64, message: t('form:error.tooLong') },
              ],
            })(<Input placeholder={t('emailPlaceholder')} />)
          }
        </Form.Item>
        <Form.Item label={t('showTutorialsLabel')}>
          {
            getFieldDecorator('showTutorial', {
              initialValue: user.showTutorial,
            })(<Switch defaultChecked={user.showTutorial} />)
          }
        </Form.Item>
        <Form.Item label={t('emailNotificationsLabel')}>
          {
            getFieldDecorator('receiveEmailNotifications', {
              initialValue: user.receiveEmailNotifications,
            })(<Switch defaultChecked={user.receiveEmailNotifications} />)
          }
        </Form.Item>
        <Form.Item style={{ margin: 0 }}>
          <Button
            type="primary"
            htmlType="submit"
            style={{ width: '100%' }}
            loading={isWaitingForServerResponse}
          >
            {t('saveProfileSubmit')}
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default translate(['personalProfile', 'form'])(Form.create()(ProfileEditing));
