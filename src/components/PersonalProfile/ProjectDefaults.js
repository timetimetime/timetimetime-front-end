import React, { Component } from 'react';
import {
  Form,
  Button,
  Input,
  message,
  Radio,
  InputNumber,
} from 'antd';
import { translate } from 'react-i18next';

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class ProjectDefaults extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isWaitingForServerResponse: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    const { companyId, getModels } = this.props;
    getModels('projectDefaults', { companyId });
  }
  handleSubmit(event) {
    event.preventDefault();
    const { t, projectDefaults } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.setState({ isWaitingForServerResponse: true });
        const model = Object.assign({}, values, { id: projectDefaults.id });
        this.props.updateModel('projectDefaults', model).then(() => {
          message.success(t('projectDefaultsChangedMessage'));
          this.setState({ isWaitingForServerResponse: false });
          this.props.form.resetFields();
        }).catch((error) => {
          console.error(error);
          message.error(t('form:error.unknown'));
          this.setState({ isWaitingForServerResponse: false });
        });
      }
    });
  }
  render() {
    const { isWaitingForServerResponse } = this.state;
    const { t, projectDefaults } = this.props;
    const {
      getFieldDecorator,
    } = this.props.form;
    return projectDefaults ?
      <Form onSubmit={this.handleSubmit}>
        <Form.Item label={t('estimation:projectInsuranceLabel')}>
          {
            getFieldDecorator('insurance', {
              initialValue: projectDefaults.insurance,
              rules: [
                { required: true, message: t('form:error.required') },
                { type: 'integer', message: t('form:error.isNotInteger') },
              ],
            })(<InputNumber min={0} max={1000} style={{ width: '100%' }} placeholder={t('estimation:projectInsurancePlaceholder')} />)
          }
        </Form.Item>
        <Form.Item label={t('estimation:projectIncomeLabel')}>
          {
            getFieldDecorator('income', {
              initialValue: projectDefaults.income,
              rules: [
                { required: true, message: t('form:error.required') },
                { type: 'integer', message: t('form:error.isNotInteger') },
              ],
            })(<InputNumber min={0} max={1000} style={{ width: '100%' }} placeholder={t('estimation:projectIncomePlaceholder')} />)
          }
        </Form.Item>
        <Form.Item label={t('estimation:projectCommissionLabel')}>
          {
            getFieldDecorator('salesmanCommission', {
              initialValue: projectDefaults.salesmanCommission,
              rules: [
                { required: true, message: t('form:error.required') },
                { type: 'integer', message: t('form:error.isNotInteger') },
              ],
            })(<InputNumber min={0} max={1000} style={{ width: '100%' }} placeholder={t('estimation:projectCommissionPlaceholder')} />)
          }
        </Form.Item>
        <Form.Item label={t('estimation:salesmanCommissionBaseLabel')}>
          {
            getFieldDecorator('salesmanCommissionBase', {
              initialValue: projectDefaults.salesmanCommissionBase,
              rules: [
                { required: true, message: t('form:error.required') },
              ],
            })(<RadioGroup style={{ width: '100%' }}>
              <RadioButton value="total" style={{ width: '50%', textAlign: 'center' }}>
                { t('estimation:commissionFromTotal') }
              </RadioButton>
              <RadioButton value="income" style={{ width: '50%', textAlign: 'center' }}>
                { t('estimation:commissionFromIncome') }
              </RadioButton>
            </RadioGroup>)
          }
        </Form.Item>
        <Form.Item label={t('estimation:projectTaxesLabel')}>
          {
            getFieldDecorator('taxes', {
              initialValue: projectDefaults.taxes,
              rules: [
                { required: true, message: t('form:error.required') },
                { type: 'integer', message: t('form:error.isNotInteger') },
              ],
            })(<InputNumber min={0} max={1000} style={{ width: '100%' }} placeholder={t('estimation:projectTaxesPlaceholder')} />)
          }
        </Form.Item>
        <Form.Item style={{ margin: 0 }}>
          <Button
            type="primary"
            htmlType="submit"
            style={{ width: '100%' }}
            loading={isWaitingForServerResponse}
          >
            {t('changePasswordSubmit')}
          </Button>
        </Form.Item>
      </Form>
      : null;
  }
}

export default translate(['personalProfile', 'form', 'estimation'])(Form.create()(ProjectDefaults));
