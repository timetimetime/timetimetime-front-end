import React, { Component } from 'react';
import {
  Form,
  Button,
  Input,
  Radio,
  Select,
  message,
} from 'antd';
import { translate } from 'react-i18next';
import { Redirect } from 'react-router-dom';

const getSlug = require('speakingurl');

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class CreateProject extends Component {
  constructor(props) {
    super(props);
    this.state = {
      redirectTo: null,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    const { companyId, getServices } = this.props;
    getServices({ companyId });
  }
  handleSubmit(event) {
    event.preventDefault();
    const { t, companyId, companyUri } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const uri = getSlug(values.name);
        const model = Object.assign({}, values, {
          companyId,
          uri,
        });
        this.props.createProject(model).then(() => {
          this.setState({
            redirectTo: `/${companyUri}/${uri}`,
          });
          // this.props.form.resetFields();
        }).catch((error) => {
          if (error.code === 400 && error.message === 'Validation error') {
            const validationError = error.errors[0];
            if (validationError.type === 'unique violation' && validationError.path === 'companyId') {
              message.error(t('uriIsNotUnique'));
            }
            return true;
          } else if (error.code === 403 && error.message === 'PAYMENT_REQUIRED') {
            message.warning(t('projectNumberLimit'));
            return true;
          }
          message.error(t('form:error.unknown'));
          return error;
        });
      }
    });
  }
  render() {
    const { services, t } = this.props;
    const { redirectTo } = this.state;
    const {
      getFieldDecorator,
      getFieldsError,
    } = this.props.form;
    let initialService = services.length === 1 ? services[0].id : undefined;
    return (
      <Form onSubmit={this.handleSubmit} hideRequiredMark>
        { redirectTo ? <Redirect push to={redirectTo} /> : null }
        <Form.Item label={t('nameInputLabel')}>
          {
            getFieldDecorator('name', {
              rules: [
                { required: true, message: t('form:error.required') },
                { min: 1, message: t('form:error.tooShort') },
                { max: 64, message: t('form:error.tooLong') },
              ],
            })(<Input placeholder={t('nameInputPlaceholder')} />)
          }
        </Form.Item>
        <Form.Item label={t('serviceInputLabel')}>
          {
            getFieldDecorator('serviceId', {
              initialValue: initialService,
              rules: [
                { required: true, message: t('form:error.required') },
              ],
            })(
              <Select
                style={{ width: '100%' }}
                placeholder={t('serviceInputPlaceholder')}
              >
                {
                  services.map(service =>
                    (
                      <Select.Option
                        key={service.id}
                        value={service.id}
                      >
                        {service.name}
                      </Select.Option>
                    )
                  )
                }
              </Select>
            )
          }
        </Form.Item>
        <Form.Item>
          {
            getFieldDecorator('type', {
              initialValue: 'agile',
              rules: [
                { required: true, message: t('form:error.required') },
              ],
            })(<RadioGroup style={{ width: '100%' }}>
              <RadioButton value="agile" style={{ width: '50%', textAlign: 'center' }}>
                  Agile
              </RadioButton>
              <RadioButton value="waterfall" style={{ width: '50%', textAlign: 'center' }}>
                  Waterfall
              </RadioButton>
            </RadioGroup>)
          }
        </Form.Item>
        <Form.Item style={{ margin: 0 }}>
          <Button
            type="primary"
            htmlType="submit"
            disabled={hasErrors(getFieldsError())}
            style={{ width: '100%' }}
          >
            {t('creationSubmit')}
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default translate(['projects', 'form'])(Form.create()(CreateProject));
