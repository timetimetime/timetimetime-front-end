import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './CreateProject';
import { createModel, getModels } from '../../actions/models';

function mapStateToProps(state) {
  const companyId = state.current.company;
  return {
    companyId,
    services: state.services.filter(service => service.companyId === companyId),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createProject: model => createModel('projects', model),
    getServices: params => getModels('services', params),
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
