import React from 'react';
import { Steps } from 'antd';
import { translate } from 'react-i18next';
import { Link } from 'react-router-dom';

const { Step } = Steps;

const GoTo = ({ page, uri, t }) => (
  <p>
    <Link to={`/${uri}/${page}`}>
      {t('goToEntering')}
    </Link>
  </p>
);

const GettingStarted = ({
  t,
  companyUri,
  hasServices,
  hasSubtaskCategories,
  hasPerformers,
  hasExpenses,
}) => (
  <div>
    <h2>
      {t('title')}:
    </h2>
    <Steps direction="vertical" progressDot>
      <Step
        title={t('servicesStep')}
        status={hasServices ? 'finish' : 'process'}
        description={
          hasServices ? null : <GoTo page="product-types" uri={companyUri} t={t} />
        }
      />
      <Step
        title={t('subtaskCategoriesStep')}
        status={
          hasSubtaskCategories ? 'finish' :
            (hasServices) ? 'process' : 'wait'
        }
        description={
          (!hasSubtaskCategories && hasServices) ? <GoTo page="work-categories" uri={companyUri} t={t} /> : null
        }
      />
      <Step
        title={t('performersStep')}
        status={
          hasPerformers ? 'finish' :
            (hasServices && hasSubtaskCategories) ? 'process' : 'wait'
        }
        description={
          (!hasPerformers && hasSubtaskCategories) ? <GoTo page="performers" uri={companyUri} t={t} /> : null
        }
      />
      <Step
        title={t('expensesStep')}
        status={
          hasExpenses ? 'finish' :
            (hasServices && hasSubtaskCategories && hasPerformers) ? 'process' : 'wait'
        }
        description={
          (!hasExpenses && hasPerformers) ? <GoTo page="expenses" uri={companyUri} t={t} /> : null
        }
      />
    </Steps>
  </div>
);

export default translate(['gettingStarted'])(GettingStarted);
