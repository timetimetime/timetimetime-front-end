import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './ProjectsList';
import { getModels, deleteModel } from '../../actions/models';

function mapStateToProps(state) {
  const companyId = state.current.company;
  return {
    projects: state.projects.filter(project => project.companyId === companyId),
    companyId,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getModels,
    deleteModel,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
