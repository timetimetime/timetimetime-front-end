import React from 'react';
import { Row, Col, Card } from 'antd';
import CreateProject from './CreateProjectContainer';
import ProjectsList from './ProjectListContainer';

export default ({ match, t }) => (
  <Row gutter={16}>
    <Col span={18}>
      <ProjectsList companyUri={match.params.companyUri} />
    </Col>
    <Col span={6}>
      <Card title={t('createProjectTitle')}>
        <CreateProject companyUri={match.params.companyUri} />
      </Card>
    </Col>
  </Row>
);
