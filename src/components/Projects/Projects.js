import React from 'react';
import { translate } from 'react-i18next';
import InnerPageLayoutContainer from '../InnerPageLayout/InnerPageLayoutContainer';
import Tutorial from '../Tutorial/TutorialContainer';
import ProjectsContent from './ProjectsContentContainer';


const Projects = ({ t, match }) => (
  <InnerPageLayoutContainer
    page="projects"
    companyUri={match.params.companyUri}
    title={t('title')}
  >
    <Tutorial theme="projects" />
    <ProjectsContent t={t} match={match} companyUri={match.params.companyUri} />
  </InnerPageLayoutContainer>
);

export default translate(['projects'])(Projects);
