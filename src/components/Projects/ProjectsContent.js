import React, { Component } from 'react';
import { Spin } from 'antd';
import ProjectManagement from './ProjectManagement';
import GettingStarted from './GettingStarted';
import StatusWrapper from '../StatusWrapper/StatusWrapper';

export default class ProjectsContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isDataLoaded: false,
      servicesNumber: null,
      subtaskCategoriesNumber: null,
      performersNumber: null,
      expensesNumber: null,
    };
  }
  componentDidMount() {
    const {
      companyId,
      getServices,
      getSubtaskCategories,
      getPerformers,
      getExpenses,
    } = this.props;
    const servicesRequest = getServices({ companyId }).then((res) => {
      this.setState({ servicesNumber: res.total });
    });
    const subtaskCategoriesRequest = getSubtaskCategories({ companyId }).then((res) => {
      this.setState({ subtaskCategoriesNumber: res.total });
    });
    const performersRequest = getPerformers({ companyId }).then((res) => {
      this.setState({ performersNumber: res.total });
    });
    const expensesRequest = getExpenses({ companyId }).then((res) => {
      this.setState({ expensesNumber: res.total });
    });
    const requests = [
      servicesRequest,
      subtaskCategoriesRequest,
      performersRequest,
      expensesRequest,
    ];
    Promise.all(requests).then(() => {
      this.setState({ isDataLoaded: true });
    });
  }
  render() {
    const {
      isDataLoaded,
      servicesNumber,
      subtaskCategoriesNumber,
      performersNumber,
      expensesNumber,
    } = this.state;
    const {
      t,
      match,
      companyUri,
    } = this.props;
    if (!isDataLoaded) {
      return (
        <StatusWrapper height="328px">
          <Spin />
        </StatusWrapper>
      );
    }
    const hasDataForCalculations =
      servicesNumber > 0
      && subtaskCategoriesNumber > 0
      && performersNumber > 0
      && expensesNumber > 0;
    if (hasDataForCalculations) {
      return (
        <ProjectManagement t={t} match={match} />
      );
    }
    return (
      <GettingStarted
        companyUri={companyUri}
        hasServices={servicesNumber > 0}
        hasSubtaskCategories={subtaskCategoriesNumber > 0}
        hasPerformers={performersNumber > 0}
        hasExpenses={expensesNumber > 0}
      />
    );
  }
}
