import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './ProjectsContent';
import { getModels } from '../../actions/models';

function mapStateToProps(state) {
  const companyId = state.current.company;
  return {
    companyId,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getServices: params => getModels('services', params),
    getSubtaskCategories: params => getModels('subtaskCategories', params),
    getPerformers: params => getModels('performers', params),
    getExpenses: params => getModels('expenses', params),
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
