import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { List, Spin, Button, Popconfirm, message } from 'antd';
import { translate } from 'react-i18next';

const StatusWrapper = ({ children }) => (
  <div
    style={{
      height: '306px',
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    }}
  >
    { children }
  </div>
);

class ProjectsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      records: [],
      isProjectListLoaded: false, // first load
      isPageIsLoading: false,
      total: 0,
      limit: 10,
    };
    this.loadNextPage = this.loadNextPage.bind(this);
  }
  componentDidMount() {
    const { companyId, getModels } = this.props;
    getModels('projects', { companyId, $limit: this.state.limit }).then((response) => {
      this.setState({
        isProjectListLoaded: true,
        total: response.total,
        records: response.data,
      });
    });
  }
  componentWillReceiveProps(nextProps) {
    const propWithRecords = 'projects';
    const hasDifferentRecordsNumber = this.props[propWithRecords].length !== nextProps[propWithRecords].length;
    const itIsNotInitialization = this.state.isProjectListLoaded === true;
    const itIsNotPageLoading = this.state.isPageIsLoading === false;
    if (hasDifferentRecordsNumber && itIsNotInitialization && itIsNotPageLoading) {
      this.componentDidMount();
    }
  }
  loadNextPage() {
    const { companyId, getModels } = this.props;
    const { records } = this.state;
    this.setState({ isPageIsLoading: true });
    getModels('projects', { companyId, $limit: this.state.limit, $skip: records.length }).then((response) => {
      this.setState({
        isPageIsLoading: false,
        total: response.total,
        records: this.state.records.concat(response.data),
      });
    });
  }
  handleDelete(id) {
    const { deleteModel, t } = this.props;
    deleteModel('projects', id).then(() => {
      message.success(t('projectDeletedMessage'));
    }).catch((error) => {
      if (error.response) {
        message.error('form:error:unknown');
      } else {
        throw error;
      }
    });
  }
  render() {
    const { t, companyUri } = this.props;
    const {
      isProjectListLoaded,
      isPageIsLoading,
      total,
      records,
    } = this.state;
    let componentToRender;
    if (!isProjectListLoaded) {
      componentToRender = (
        <StatusWrapper>
          <Spin />
        </StatusWrapper>
      );
    } else if (isProjectListLoaded && records.length > 0) {
      const loadMore = total > records.length ? (
        <div
          style={{
            textAlign: 'center', padding: '12px 0', backgroundColor: '#fff',
          }}
        >
          {isPageIsLoading && <Spin />}
          {!isPageIsLoading && <Button onClick={this.loadNextPage}>{t('getNextProjects')}</Button>}
        </div>
      ) : null;
      componentToRender = (
        <List
          bordered
          dataSource={records}
          loadMore={loadMore}
          loading={isPageIsLoading}
          renderItem={project => (
            <List.Item
              actions={[
                <Popconfirm
                  title={t('confirmDeleteMessage')}
                  okText={t('commonui:confirm')}
                  cancelText={t('commonui:cancel')}
                  onConfirm={() => this.handleDelete(project.id)}
                >
                  <Button type="danger" ghost>
                    {t('deleteProjectButton')}
                  </Button>
                </Popconfirm>,
                <Link to={`/${companyUri}/${project.uri}`}>
                  <Button>{t('linkToEstimation')}</Button>
                </Link>,
              ]}
              style={{ backgroundColor: '#fff' }}
            >
              <List.Item.Meta
                title={project.name}
              />
            </List.Item>)
          }
        />
      );
    } else {
      componentToRender = (
        <StatusWrapper>
          <p>{t('noProjects')}</p>
        </StatusWrapper>
      );
    }
    return componentToRender;
  }
}

export default translate(['projects', 'form', 'commonui'])(ProjectsList);
