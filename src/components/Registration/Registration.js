import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import {
  Form,
  Button,
  Input,
  Checkbox,
  message,
  Row,
  Col,
  Card,
} from 'antd';
import { translate } from 'react-i18next';
import OuterPageLayout from '../OuterPageLayout/OuterPageLayoutContainer';

class Registration extends Component {
  constructor(props) {
    super(props);
    this.state = {
      registered: false,
      isWaitingForServerResponse: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.registration = this.registration.bind(this);
  }
  async registration(values) {
    const { t, createModel, authenticate } = this.props;
    this.setState({ isWaitingForServerResponse: true });
    try {
      const user = await createModel('users', values);
      await authenticate(user.email, values.password);
      this.setState({
        registered: true,
        isWaitingForServerResponse: false,
      });
      return true;
    } catch (error) {
      this.setState({ isWaitingForServerResponse: false });
      if (error.code === 400 && error.message === 'Validation error') {
        const validationError = error.errors[0];
        if (validationError.type === 'unique violation' && validationError.path === 'email') {
          message.error(t('userEmailNotUnique'));
        }
        return true;
      } else if (error.code === 403 && error.message === 'TOO_MANY_REGISTRATIONS_PER_IP') {
        message.warning(t('tooManyRegistrations'));
        return true;
      }
      message.error(t('form:error.unknown'));
      return error;
    }
  }
  handleSubmit(event) {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.registration(values);
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { t } = this.props;
    const { isWaitingForServerResponse } = this.state;
    return (
      <OuterPageLayout page="registration">
        <div className="container">
          <Row type="flex" justify="center">
            <Col span={7}>
              <Card title={t('title')}>
                <Form
                  layout="vertical"
                  style={{ width: '100%' }}
                  onSubmit={this.handleSubmit}
                  hideRequiredMark
                >
                  { this.state.registered ? <Redirect push to="/create-company" /> : null }
                  <Form.Item label={t('emailLabel')}>
                    {
                      getFieldDecorator('email', {
                        validateTrigger: 'onBlur',
                        rules: [
                          { required: true, message: t('form:error.required') },
                          { type: 'email', message: t('form:error.emailFormat') },
                        ],
                      })(<Input autoFocus placeholder={t('email')} />)
                    }
                  </Form.Item>
                  <Form.Item label={t('passwordLabel')}>
                    {
                      getFieldDecorator('password', {
                        validateTrigger: 'onBlur',
                        rules: [
                          { required: true, message: t('form:error.required') },
                          { min: 7, message: t('minPasswordLength') },
                          { max: 48, message: t('form:error.tooLong') },
                        ],
                      })(<Input type="password" placeholder={t('password')} />)
                    }
                  </Form.Item>
                  {/*
                    <Form.Item label={t('firstNameLabel')}>
                      {
                        getFieldDecorator('firstName', {
                          validateTrigger: 'onBlur',
                          rules: [
                            { required: true, message: t('form:error.required') },
                            { min: 1, message: t('form:error.tooShort') },
                            { max: 64, message: t('form:error.tooLong') },
                          ],
                        })(<Input type="text" placeholder={t('firstName')} />)
                      }
                    </Form.Item>
                    <Form.Item label={t('lastNameLabel')}>
                      {
                        getFieldDecorator('lastName', {
                          validateTrigger: 'onBlur',
                          rules: [
                            { required: true, message: t('form:error.required') },
                            { min: 1, message: t('form:error.tooShort') },
                            { max: 64, message: t('form:error.tooLong') },
                          ],
                        })(<Input type="text" placeholder={t('lastName')} />)
                      }
                    </Form.Item>
                  */}
                  {/*
                    <Form.Item>
                      {
                        getFieldDecorator('accept', {
                          initialValue: false,
                          rules: [
                            {
                              pattern: /true/,
                              message: t('termsAreNotAccepted'),
                            },
                          ],
                        })(
                          <Checkbox style={{ fontSize: '10px' }}>
                            {t('acceptTermsText')}
                          </Checkbox>
                        )
                      }
                    </Form.Item>
                  */}
                  <p>
                    <small>
                      Регистрируясь, вы соглашаетесь с условиями <a href="/user-agreement" target="_blank">Пользовательского соглашения</a> и <a href="/privacy-policy" target="_blank">Политики конфиденциальности</a>
                    </small>
                  </p>
                  <Form.Item style={{ margin: 0 }}>
                    <Button
                      type="primary"
                      htmlType="submit"
                      style={{ width: '100%' }}
                      loading={isWaitingForServerResponse}
                    >
                      {t('submit')}
                    </Button>
                  </Form.Item>
                </Form>
              </Card>
            </Col>
          </Row>
        </div>
      </OuterPageLayout>
    );
  }
}

export default translate(['registration', 'form'])(Form.create()(Registration));
