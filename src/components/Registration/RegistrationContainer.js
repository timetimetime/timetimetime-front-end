import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './Registration';
import { createModel } from '../../actions/models';
import { authenticate } from '../../actions/users';

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createModel,
    authenticate,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
