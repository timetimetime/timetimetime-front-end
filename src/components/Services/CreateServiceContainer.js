import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './CreateService';
import { createModel } from '../../actions/models';

function mapStateToProps(state) {
  return {
    companyId: state.current.company,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    createService: model => createModel('services', model),
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
