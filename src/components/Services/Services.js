import React from 'react';
import { translate } from 'react-i18next';
import { Row, Col, Card } from 'antd';
import Tutorial from '../Tutorial/TutorialContainer';
import CreateService from './CreateServiceContainer';
import ServicesList from './ServicesListContainer';
import InnerPageLayoutContainer from '../InnerPageLayout/InnerPageLayoutContainer';

const Services = ({ match, t }) => (
  <InnerPageLayoutContainer
    page="services"
    companyUri={match.params.companyUri}
    title={t('title')}
  >
    <Tutorial theme="services" />
    <Row gutter={16}>
      <Col span={18}>
        <ServicesList />
      </Col>
      <Col span={6}>
        <Card title={t('addServiceTitle')}>
          <CreateService />
        </Card>
      </Col>
    </Row>
  </InnerPageLayoutContainer>
);

export default translate(['services'])(Services);
