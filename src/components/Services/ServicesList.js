import React from 'react';
import { translate } from 'react-i18next';
import DataGrid from '../DataGrid/DataGrid';

function getColumns(t) {
  return [
    {
      title: t('nameColumn'),
      dataIndex: 'name',
      width: '75%',
    },
  ];
}

function getTextKeys(t) {
  return {
    emptyText: t(),
  };
}

const ServicesList = ({
  t,
  services,
  getServices,
  companyId,
  deleteService,
  updateService,
}) => (
  <DataGrid
    columns={getColumns(t)}
    textKeys={getTextKeys(t)}
    records={services}
    getRecords={params => getServices(Object.assign({}, params, { companyId }))}
    deleteRecord={deleteService}
    updateRecord={updateService}
  />
);

export default translate(['services', 'form', 'commonui'])(ServicesList);
