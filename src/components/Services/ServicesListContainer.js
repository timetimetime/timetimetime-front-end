import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './ServicesList';
import { getModels, updateModel, deleteModel } from '../../actions/models';

function mapStateToProps(state) {
  const companyId = state.current.company;
  return {
    services: state.services.filter(service => service.companyId === companyId),
    companyId,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getServices: params => getModels('services', params),
    updateService: model => updateModel('services', model),
    deleteService: id => deleteModel('services', id),
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
