import React, { Component } from 'react';
import {
  Form,
  Button,
  Input,
  message,
  Row,
  Col,
  Card,
} from 'antd';
import { translate } from 'react-i18next';
import OuterPageLayout from '../OuterPageLayout/OuterPageLayoutContainer';

const queryString = require('query-string');

class SetNewPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isWaitingForServerResponse: false,
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(event) {
    event.preventDefault();
    const { t } = this.props;
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const query = queryString.parse(this.props.location.search);
        const data = { confirmationCode: query.code, password: values.password };
        this.setState({ isWaitingForServerResponse: true });
        this.props.completePasswordRecovery(data).then(() => {
          this.setState({ isWaitingForServerResponse: false });
          message.success(t('passwordChanged'));
        }).catch((error) => {
          this.setState({ isWaitingForServerResponse: false });
          if (error.code === 400) {
            if (error.message === 'NO_PASSWORD_RECOVERY') {
              message.error(t('cannotChangePassword'));
              return true;
            }
          }
          message.error(t('form:error.unknown'));
        });
      }
    });
  }
  render() {
    const { getFieldDecorator } = this.props.form;
    const { t } = this.props;
    const { isWaitingForServerResponse } = this.state;
    return (
      <OuterPageLayout page="set-new-password">
        <div className="container">
          <Row>
            <Col span={6} offset={9}>
              <Card title={t('setNewPasswordTitle')}>
                <Form
                  layout="vertical"
                  style={{ width: '100%' }}
                  onSubmit={this.handleSubmit}
                >
                  <Form.Item>
                    {
                      getFieldDecorator('password', {
                        rules: [
                          { required: true, message: t('form:error.required') },
                        ],
                      })(<Input type="password" autoFocus placeholder={t('newPasswordPlaceholder')} />)
                    }
                  </Form.Item>
                  <Form.Item style={{ margin: 0 }}>
                    <Button
                      type="primary"
                      htmlType="submit"
                      style={{ width: '100%' }}
                      loading={isWaitingForServerResponse}
                    >
                      {t('submitNewPassword')}
                    </Button>
                  </Form.Item>
                </Form>
              </Card>
            </Col>
          </Row>
        </div>
      </OuterPageLayout>
    );
  }
}

export default translate(['passwordRecovery', 'form'])(Form.create()(SetNewPassword));
