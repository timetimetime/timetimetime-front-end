import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './SetNewPassword';
import { completePasswordRecovery } from '../../actions/users';

function mapStateToProps() {
  return {};
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    completePasswordRecovery,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
