import React from 'react';

export default () => (
  <div className="sorry">
    <p>
      Извините, наш сервис не оптимизирован для использования на маленьких экранах :(
    </p>
  </div>
);
