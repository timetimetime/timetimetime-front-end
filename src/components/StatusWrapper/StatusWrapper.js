import React from 'react';

const statusWrapperStyles = {
  height: '306px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
};

const StatusWrapper = ({ children, height }) => (
  <div
    style={Object.assign({}, statusWrapperStyles, { height })}
  >
    { children }
  </div>
);

export default StatusWrapper;
