import React, { Component } from 'react';
import {
  Form,
  Button,
  Input,
  Select,
  message,
  Row,
  Col,
} from 'antd';
import { translate } from 'react-i18next';

class CategoryEditing extends Component {
  constructor(props) {
    super(props);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleDelete() {
    const { t } = this.props;
    this.props.deleteModel('subtaskCategories', this.props.recordId).then(() => {
      message.success(t('categoryDeletedMessage'));
    }).catch(() => {
      message.error(t('form:error.unknown'));
    });
  }
  handleSubmit(event) {
    event.preventDefault();
    const { t } = this.props;
    // const addPredecessors = (updatedModel, data) => {
    //   if (data.predecessors) {
    //     updatedModel.predecessors = data.predecessors.map(el => Object.assign({}, {
    //       id: el,
    //     }));
    //   }
    // };
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const model = Object.assign({}, values, { id: this.props.recordId });
        this.props.updateModel('subtaskCategories', model/*, { afterUpdate: addPredecessors }*/).then(() => {
          message.success(t('categorySavedMessage'));
        }).catch((error) => {
          message.error(t('form:error.unknown'));
          console.error(error);
        });
      }
    });
  }
  render() {
    const { t, subtaskCategories, recordId } = this.props;
    const {
      getFieldDecorator,
    } = this.props.form;
    const record = subtaskCategories.filter(el => el.id === recordId)[0];
    return (
      <Form onSubmit={this.handleSubmit} style={this.props.style}>
        <Row gutter={16}>
          <Col span={6}>
            <Form.Item style={{ margin: 0 }}>
              {
                getFieldDecorator('name', {
                  initialValue: record.name,
                  rules: [
                    { required: true, message: 'Enter name please' },
                    { min: 1, message: 'Too short' },
                    { max: 64, message: 'Too long' },
                  ],
                })(<Input placeholder={t('categoryNamePlaceholder')} />)
              }
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item style={{ margin: 0 }}>
              {
                getFieldDecorator('predecessors', {
                  initialValue: record.predecessors,
                })(
                  <Select
                    style={{ width: '100%' }}
                    placeholder={t('predecessorsPlaceholder')}
                    mode="multiple"
                  >
                    {
                      subtaskCategories.filter(el => el.id !== record.id).map(subtaskCategory => (
                        <Select.Option
                          key={subtaskCategory.id}
                          value={subtaskCategory.id}
                        >
                          {subtaskCategory.name}
                        </Select.Option>
                      ))
                    }
                  </Select>
                )
              }
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item style={{ margin: 0 }}>
              <Button
                type="primary"
                htmlType="submit"
                style={{ width: '100%' }}
              >
                {t('saveCategorySubmit')}
              </Button>
            </Form.Item>
          </Col>
          <Col span={6}>
            <Form.Item style={{ margin: 0 }}>
              <Button
                type="danger"
                htmlType="button"
                onClick={this.handleDelete}
                style={{ width: '100%' }}
                ghost
              >
                {t('deleteCategoryButton')}
              </Button>
            </Form.Item>
          </Col>
        </Row>
      </Form>
    );
  }
}

export default translate(['subtaskCategories', 'form'])(Form.create()(CategoryEditing));
