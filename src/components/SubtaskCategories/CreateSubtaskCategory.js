import React, { Component } from 'react';
import {
  Form,
  Button,
  Input,
  Select,
  message,
} from 'antd';
import { translate } from 'react-i18next';

function hasErrors(fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}

class CreateSubtaskCategory extends Component {
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentDidMount() {
    const { getSubtaskCategories, company } = this.props;
    getSubtaskCategories({ companyId: company });
  }
  handleSubmit(event) {
    event.preventDefault();
    const { t } = this.props;
    // const addPredecessors = (model, data) => {
    //   if (data.predecessors) {
    //     model.predecessors = data.predecessors.map(el => Object.assign({}, {
    //       id: el,
    //     }));
    //   } else {
    //     model.predecessors = [];
    //   }
    // };
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const model = Object.assign({}, values, {
          companyId: this.props.company,
          serviceId: this.props.selectedService,
        });
        this.props.createSubtaskCategory(model/*, { afterCreate: addPredecessors }*/).then(() => {
          message.success(t('categoryCreatedMessage'));
          this.props.form.resetFields();
        }).catch((error) => {
          if (error.code === 403 && error.message === 'MODELS_NUMBER_LIMIT') {
            message.warning(t('modelsNumberLimit'));
            return true;
          }
          message.error(t('form:error.unknown'));
          return error;
        });
      }
    });
  }
  render() {
    const { t, subtaskCategories } = this.props;
    const {
      getFieldDecorator,
      getFieldsError,
    } = this.props.form;
    return (
      <Form id="create-model-form" onSubmit={this.handleSubmit} hideRequiredMark>
        <Form.Item label={t('categoryNameLabel')}>
          {
            getFieldDecorator('name', {
              rules: [
                { required: true, message: t('form:error.required') },
                { min: 1, message: t('form:error.tooShort') },
                { max: 64, message: t('form:error.tooLong') },
              ],
            })(<Input placeholder={t('categoryNamePlaceholder')} />)
          }
        </Form.Item>
        <Form.Item label={t('predecessorsNameLabel')}>
          {
            getFieldDecorator('predecessors')(
              <Select
                style={{ width: '100%' }}
                placeholder={t('predecessorsPlaceholder')}
                mode="multiple"
                dropdownClassName="predecessors-select"
              >
                {
                  subtaskCategories.map(subtaskCategory =>
                    <Select.Option
                      key={subtaskCategory.id}
                      value={subtaskCategory.id}
                    >
                      {subtaskCategory.name}
                    </Select.Option>
                  )
                }
              </Select>
            )
          }
        </Form.Item>
        <Form.Item style={{ margin: 0 }}>
          <Button
            type="primary"
            htmlType="submit"
            disabled={hasErrors(getFieldsError())}
            style={{ width: '100%' }}
          >
            {t('createCategorySubmit')}
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

export default translate(['subtaskCategories', 'form'])(Form.create()(CreateSubtaskCategory));
