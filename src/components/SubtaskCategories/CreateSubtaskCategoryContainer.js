import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './CreateSubtaskCategory';
import { getModels, createModel } from '../../actions/models';

function mapStateToProps(state) {
  const companyId = state.current.company;
  return {
    subtaskCategories: state.subtaskCategories.filter(category => category.companyId === companyId),
    services: state.services.filter(service => service.companyId === companyId),
    company: companyId,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    getSubtaskCategories: params => getModels('subtaskCategories', params),
    createSubtaskCategory: (model, options) => createModel('subtaskCategories', model, options),
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
