import React, { Component } from 'react';
import { Select, Form } from 'antd';
import { translate } from 'react-i18next';

const { Option } = Select;

class ServiceSelection extends Component {
  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount() {
    const { companyId, getModels } = this.props;
    getModels('services', { companyId }).then((services) => {
      if (services.data.length > 0) {
        this.handleChange(services.data[0].id);
      }
    });
  }
  handleChange(value) {
    this.props.onSelect(value);
  }
  render() {
    const { services, selectedService, t } = this.props;
    return (
      <Form layout="inline" style={{ marginBottom: '16px' }}>
        <Form.Item label={t('selectServiceLabel')}>
          <Select
            style={{ width: '240px' }}
            onChange={this.handleChange}
            placeholder={t('selectServicePlaceholder')}
            value={selectedService}
          >
            {
              services.map(el => <Option key={el.id} value={el.id}>{el.name}</Option>)
            }
          </Select>
        </Form.Item>
      </Form>
    );
  }
}

export default translate(['subtaskCategories'])(ServiceSelection);
