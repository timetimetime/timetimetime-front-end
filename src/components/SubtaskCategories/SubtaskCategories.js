import React, { Component } from 'react';
import { translate } from 'react-i18next';
import { Row, Col, Card } from 'antd';
import Tutorial from '../Tutorial/TutorialContainer';
import CreateSubtaskCategory from './CreateSubtaskCategoryContainer';
import SubtaskCategoriesList from './SubtaskCategoriesListContainer';
import InnerPageLayoutContainer from '../InnerPageLayout/InnerPageLayoutContainer';
import ServiceSelection from './ServiceSelection';

class SubtaskCategories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedService: null,
    };
    this.onServiceSelect = this.onServiceSelect.bind(this);
  }
  onServiceSelect(selectedService) {
    this.setState({ selectedService });
  }
  render() {
    const { match, t } = this.props;
    const { selectedService } = this.state;
    return (
      <InnerPageLayoutContainer
        page="subtaskCategories"
        companyUri={match.params.companyUri}
        title={t('title')}
      >
        <Tutorial theme="subtaskCategories" />
        <ServiceSelection
          {...this.props}
          onSelect={this.onServiceSelect}
          selectedService={selectedService}
        />
        {
          selectedService ?
            <Row gutter={16}>
              <Col span={18}>
                <SubtaskCategoriesList selectedService={selectedService} />
              </Col>
              <Col span={6}>
                <Card title={t('createSubtaskCategoryTitle')}>
                  <CreateSubtaskCategory selectedService={selectedService} />
                </Card>
              </Col>
            </Row>
          : null
        }
      </InnerPageLayoutContainer>
    );
  }
}

export default translate(['subtaskCategories'])(SubtaskCategories);
