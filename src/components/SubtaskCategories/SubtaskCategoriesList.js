import React from 'react';
import Visualization from './Visualization';

const SubtaskCategoriesList = ({ subtaskCategories, updateModel, deleteModel }) => (
  <div>
    <Visualization
      subtaskCategories={subtaskCategories}
      updateModel={updateModel}
      deleteModel={deleteModel}
    />
  </div>
);

export default SubtaskCategoriesList;
