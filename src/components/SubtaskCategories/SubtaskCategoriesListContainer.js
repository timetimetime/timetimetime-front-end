import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './SubtaskCategoriesList';
import { updateModel, deleteModel } from '../../actions/models';

function mapStateToProps(state, ownProps) {
  const { selectedService } = ownProps;
  return {
    subtaskCategories: state.subtaskCategories
      .filter(el => el.serviceId === selectedService),
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    updateModel,
    deleteModel,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
