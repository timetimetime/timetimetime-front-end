import React, { Component } from 'react';
import Graph from 'react-graph-vis';
import { Card } from 'antd';
import { translate } from 'react-i18next';
import CategoryEditing from './CategoryEditing';

class Visualization extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedNode: null,
    };
  }
  componentDidUpdate() {
    if (this.graph) {
      this.graph.forceUpdate();
    }
  }
  getNodes() {
    const { subtaskCategories } = this.props;
    const nodes = subtaskCategories.map((step) => {
      return {
        id: step.id,
        label: step.name,
      };
    });
    return nodes;
  }
  getEdges() {
    const { subtaskCategories } = this.props;
    const edges = [];
    subtaskCategories.forEach((step) => {
      if (step.predecessors.length > 0) {
        step.predecessors.forEach((predecessor) => {
          edges.push({
            from: predecessor,
            to: step.id,
          });
        });
      }
    });
    return edges;
  }
  render() {
    const { t, subtaskCategories } = this.props;
    const { selectedNode } = this.state;
    const graph = {
      nodes: this.getNodes(),
      edges: this.getEdges(),
    };
    const options = {
      autoResize: true,
      height: '300px',
      width: '100%',
      nodes: {
        shape: 'box',
        shapeProperties: {
          borderRadius: 0,
        },
      },
      edges: {
        chosen: true,
      },
      layout: {
        improvedLayout: false,
        hierarchical: {
          enabled: true,
          direction: 'LR',
          sortMethod: 'directed',
          levelSeparation: 150,
          nodeSpacing: 75,
          treeSpacing: 75,
        },
      },
      interaction: {
        dragNodes: false,
        dragView: true,
        zoomView: false,
      },
      physics: {
        enabled: false,
      },
    };
    const events = {
      selectNode: event => this.setState({ selectedNode: event.nodes[0] }),
      deselectNode: () => this.setState({ selectedNode: null }),
    };
    const selectedCategory = subtaskCategories.filter(el => el.id === selectedNode)[0];
    return (
      <Card title={t('graphTitle')}>
        {
          graph.nodes.length > 0 ?
            <Graph
              graph={graph}
              options={options}
              events={events}
              style={{
                height: '300px',
                border: '1px solid #e8e8e8',
              }}
              ref={(c) => { this.graph = c; }}
            />
          :
            <p style={{ height: '300px', lineHeight: '300px', textAlign: 'center' }}>
              {t('noSubtaskCategories')}
            </p>
        }

        {
          selectedNode && selectedCategory ?
            <CategoryEditing
              recordId={selectedNode}
              subtaskCategories={subtaskCategories}
              updateModel={this.props.updateModel}
              deleteModel={this.props.deleteModel}
              style={{
                marginTop: '24px',
              }}
            />
          :
            <p
              style={{
                textAlign: 'center',
                margin: '24px 0 0 0',
                display: graph.nodes.length > 0 ? 'block' : 'none',
              }}
            >
              {t('clickToEdit')}
            </p>
        }
      </Card>
    );
  }
}

export default translate(['subtaskCategories', 'form'])(Visualization);
