import React, { Component } from 'react';
import { Alert } from 'antd';
import { translate } from 'react-i18next';

class Tutorial extends Component {
  constructor(props) {
    super(props);
    this.disableTutorials = this.disableTutorials.bind(this);
  }
  disableTutorials() {
    const { updateUser, userId } = this.props;
    updateUser({ id: userId, showTutorial: false });
  }
  render() {
    const { theme, user, t } = this.props;
    const showTutorial = user && user.showTutorial;
    return (
      showTutorial ?
        <Alert
          message={t(`${theme}.title`)}
          description={t(`${theme}.message`)}
          type="info"
          closeText={t('hide')}
          onClose={this.disableTutorials}
          showIcon
          style={{ marginBottom: '32px' }}
        />
        : null
    );
  }
}

export default translate(['tutorials'])(Tutorial);
