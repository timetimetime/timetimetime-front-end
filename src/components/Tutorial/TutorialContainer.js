import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Component from './Tutorial';
import { updateUser } from '../../actions/users';

function mapStateToProps(state) {
  const userId = state.current.user;
  return {
    user: state.users.filter(el => el.id === userId)[0],
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({
    updateUser,
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(Component);
