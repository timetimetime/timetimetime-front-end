import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import Raven from 'raven-js';
import App from './components/App/App';
import './styles/main.css';

if (process.env.NODE_ENV === 'production') {
  Raven.config('https://69e7af101a664b3d92e921972e53daff@sentry.io/285058').install();
}

clearInterval(window.pageLoadingAnimation);
const loader = document.getElementById('loading');
loader.remove();

ReactDOM.render(
  <App />,
  document.getElementById('root'),
);
