const initialState = {
  company: null,
  user: null,
  requestEmailConfirmation: true,
};

export default function currentReducer(state = initialState, action) {
  switch (action.type) {
    case 'SET_CURRENT_COMPANY':
      return Object.assign({}, state, { company: action.companyId });
    case 'SET_CURRENT_USER':
      return Object.assign({}, state, { user: action.userId });
    case 'SET_EMAIL_CONFIRMATION_REQUESTING':
      return Object.assign({}, state, { requestEmailConfirmation: action.value });
    case 'RESET_STATE':
      return Object.assign({}, initialState);
    default:
      return state;
  }
}
