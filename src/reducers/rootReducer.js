import { combineReducers } from 'redux';
import users from './users';
import roles from './roles';
import companies from './companies';
import current from './current';
import subtaskCategories from './subtaskCategories';
import expenses from './expenses';
import performers from './performers';
import projects from './projects';
import tasks from './tasks';
import subtasks from './subtasks';
import services from './services';
import projectDefaults from './projectDefaults';
import payments from './payments';
import tariffs from './tariffs';

export default combineReducers({
  users,
  roles,
  companies,
  current,
  subtaskCategories,
  expenses,
  performers,
  projects,
  tasks,
  subtasks,
  services,
  projectDefaults,
  payments,
  tariffs,
});
