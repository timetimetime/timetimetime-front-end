import { modelActionTypes } from '../actions/models';

const cH = require('../utils/collectionHelper');

const { updateElementInCollection, deleteElementFromCollectionByID, getMergedCollection } = cH;

const modelName = 'subtaskCategories';

export default function subtaskCategoriesReducer(state = [], action) {
  if (modelActionTypes.includes(action.type) && action.modelName === modelName) {
    switch (action.type) {
      case 'ADD_MODELS':
        return getMergedCollection(state, action.models);
      case 'UPDATE_MODEL':
        return updateElementInCollection(action.model, state);
      case 'REMOVE_MODEL':
        return deleteElementFromCollectionByID(action.modelId, state);
      default:
        return state;
    }
  } else {
    switch (action.type) {
      case 'RESET_STATE':
        return [];
      default:
        return state;
    }
  }
}
