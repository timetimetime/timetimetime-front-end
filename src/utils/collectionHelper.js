/**
 * Functions for work with collections (arrays, arrays of objects). Used mostly in reducers.
 * @module various/collectionHelper
 */

const _ = require('underscore');

module.exports = {
  /**
   * Merge two collections. Result contains no duplicated items (functions checks items.id for
   * uniqueness).
   * @param {array} firstCollection - Collection to merge
   * @param {array} secondCollection - Collection to merge
   * @returns {array} New merged collection
   * @todo Allow to merge any number of collections.
   */
  getMergedCollection(firstCollection, secondCollection) {
    const concatinatedCollection = firstCollection.concat(secondCollection);
    return _.uniq(concatinatedCollection, element => element.id);
  },
  /**
   * Add element to collection.
   * @param {} element - Element to add
   * @param {array} array - Target array
   * @returns {array} New array with passed element
   */
  addElementToCollection(element, array) {
    const newArray = array.map(el => el);
    newArray.push(element);
    return newArray;
  },
  /**
   * Update element in collection.
   * @param {object} element - Element to update
   * @param {array} array - Target collection (array)
   * @returns {array} New collection with updated element
   */
  updateElementInCollection(element, array) {
    const newArray = array.map((el) => {
      if (el.id === element.id) {
        return Object.assign({}, el, element);
      }
      return el;
    });
    return newArray;
  },
  /**
   * Delete item from collection by item.id
   * @param {number} id - Item id
   * @param {array} collection - Target collection
   * @returns {array} New array without element
   */
  deleteElementFromCollectionByID(id, collection) {
    const newArray = collection.filter((el) => {
      if (el.id === id) {
        return false;
      }
      return true;
    });
    return newArray;
  },
  /**
   * Change item.id
   * @param {number} from - Source item.id
   * @param {number} to - ID to set
   * @param {array} collection - Target collection
   * @returns {array} New array with updated item.id
   */
  updateElementID(from, to, collection) {
    const newArray = collection.map((el) => {
      if (el.id === from) {
        return Object.assign({}, el, { id: to });
      }
      return el;
    });
    return newArray;
  },
  /**
   * Delete item from collection.
   * @param {number} element - Deleted item's id
   * @param {array} collection - Target collection
   * @returns {array} New array without selected item
   */
  deleteElementFromCollection(element, collection) {
    const newArray = collection.filter((el) => {
      if (el.id === element) {
        return false;
      }
      return true;
    });
    return newArray;
  },
  /**
   * Set value in every collection's item.
   * @param {array} collection - Target collection
   * @param {object} update - Values to set (merge)
   * @returns {array} New array with updated values
   */
  updateValueInCollection(update, collection) {
    return collection.map(item => Object.assign({}, item, update));
  },
  /**
   * Set values to selected items
   * @param {array} idArray - Array with target elements id
   * @param {string} fieldName - Property name to set
   * @param {} fieldValue - Property value to set
   * @param {array} collection - Target collection
   * @returns {array} New array with updated items
   */
  updateElements(idArray, fieldName, fieldValue, collection) {
    return collection.map((element) => {
      if (idArray.includes(element.id)) {
        const newValues = {};
        newValues[fieldName] = fieldValue;
        return Object.assign({}, element, newValues);
      }
      return element;
    });
  },
};
