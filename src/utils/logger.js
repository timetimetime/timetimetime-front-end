import server from './server';

const jwtDecode = require('jwt-decode');

const state = {
  appOpeningHandled: false,
};

export default (sourceData) => {
  const accessToken = localStorage.getItem('accessToken');
  const userId = accessToken ? jwtDecode(accessToken).userId : null;
  const data = Object.assign({}, sourceData, {
    client: 'web',
  });
  if (userId) {
    data.userId = userId;
  }
  if (!state.appOpeningHandled && data.action === 'go_to') {
    data.action = 'open_app';
    state.appOpeningHandled = true;
  }
  server
    .service('talia-winters')
    .create(data);
};
