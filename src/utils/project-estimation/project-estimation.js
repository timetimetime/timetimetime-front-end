const _ = require('lodash');

const daysInAMonth = 30;
const workingDaysPerMonth = 22;
const holydaysPerWeek = 2;
const workingDaysPerWeek = 5;

function getLongestBranchDuration(startTask, tasks, attribute = 'descendants') {
  const relationsNumber = startTask[attribute] ? startTask[attribute].length : 0;
  if (relationsNumber > 0) {
    const relationIDs = startTask[attribute];
    const relations = tasks.filter(task => relationIDs.includes(task.id));
    const theLongestBranchDuration = relations
      .map(task => getLongestBranchDuration(task, tasks, attribute));
    return parseInt(startTask.duration, 10) + _.max(theLongestBranchDuration);
  }
  return parseInt(startTask.duration, 10);
}

function getTaskDuration(task, subtaskCategories, project) {
  let subtasksWithOrder = task.subtasks.map((subtask) => {
    const descendantCategories = subtaskCategories.filter(category =>
      category.predecessors.includes(subtask.subtaskCategoryId));
    const descendantCategoriesIds = descendantCategories.map(category => category.id);
    return Object.assign({}, subtask, {
      descendants: task.subtasks
        .filter(el => descendantCategoriesIds.includes(el.subtaskCategoryId))
        .map(el => el.id),
    });
  });
  // Handle disabled categories
  subtasksWithOrder.forEach((el) => {
    if (project.disabledSubtaskCategories.includes(el.subtaskCategoryId)) {
      el.duration = 0;
    }
  });
  task.subtasks = subtasksWithOrder; // Side effect, but... kinda useful for report.
  const startingSubtaskCategoriesIds = subtaskCategories
    .filter(category => category.predecessors.length === 0)
    .map(el => el.id);
  const startingSubtasks = subtasksWithOrder
    .filter(subtask => startingSubtaskCategoriesIds.includes(subtask.subtaskCategoryId));
  const branchDurations = startingSubtasks
    .map(subtask => getLongestBranchDuration(subtask, subtasksWithOrder));
  return branchDurations.length ? _.max(branchDurations) : 0;
}

function getTasksWithDuration(tasks, subtaskCategories, project) {
  return tasks.map(task => Object.assign({}, task, {
    duration: getTaskDuration(task, subtaskCategories, project),
  }));
}

function getPerformersWithProjectRewards(performers, totalProjectDuration) {
  return performers.map((performer) => {
    let reward;
    if (typeof totalProjectDuration !== 'undefined') {
      if (performer.workingDays > 0) {
        reward = Math.ceil((performer.salary / workingDaysPerMonth) * totalProjectDuration);
      } else {
        reward = 0;
      }
    } else {
      reward = Math.ceil((performer.salary / workingDaysPerMonth) * performer.workingDays);
    }
    return Object.assign({}, performer, { reward });
  });
}

function getInvolvedPerformersIds(tasks) {
  const involvedPerformersIds = [];
  tasks.forEach((task) => {
    task.subtasks.forEach((subtask) => {
      involvedPerformersIds.push(subtask.performerId);
    });
  });
  return involvedPerformersIds;
}

function getPhases(subtaskCategories, tasks, project) {
  const phases = subtaskCategories.map(el => ({
    id: el.id,
    subtaskCategory: el,
    subtasks: [],
    duration: 0,
  }));
  tasks.forEach((task) => {
    task.subtasks.forEach((subtask) => {
      const phase = phases.filter(el => el.id === subtask.subtaskCategoryId)[0];
      phase.subtasks.push(subtask);
      phase.duration += subtask.duration;
    });
  });
  phases.forEach((phase) => {
    if (project.disabledSubtaskCategories.includes(phase.id)) {
      phase.duration = 0;
    } else {
      phase.duration += Math.ceil((project.insurance / 100) * phase.duration);
    }
  });
  return phases;
}

function getWaterfallDuration(phases, subtaskCategories) {
  subtaskCategories.forEach((category) => {
    const phase = phases.filter(el => el.id === category.id)[0];
    const descendantCategories = subtaskCategories
      .filter(category => category.predecessors.includes(phase.subtaskCategory.id));
    const descendantCategoriesIds = descendantCategories.map(category => category.id);
    phase.descendants = descendantCategoriesIds;
  });
  const startingSubtaskCategoriesIds = subtaskCategories
    .filter(category => category.predecessors.length === 0)
    .map(el => el.id);
  const startingPhases = [];
  subtaskCategories.forEach((category) => {
    const phase = phases.filter(el => el.id === category.id)[0];
    if (startingSubtaskCategoriesIds.includes(phase.subtaskCategory.id)) {
      startingPhases.push(phase);
    }
  });
  const branchDurations = startingPhases.map(phase => getLongestBranchDuration(phase, phases));
  return branchDurations.length ? _.max(branchDurations) : 0;
}

function workingDaysToCalendar(workingDays = 0) {
  let weeksNumber = workingDays / workingDaysPerWeek;
  if (!workingDays) {
    weeksNumber = 0;
  } else if (Number.isInteger(weeksNumber)) {
    weeksNumber -= 1;
  } else {
    weeksNumber = Math.floor(weeksNumber);
  }
  const holydaysNumber = weeksNumber * holydaysPerWeek;
  const calendarDays = workingDays + holydaysNumber;
  const months = calendarDays / daysInAMonth;
  return { calendarDays, months };
}

function getProjectEstimation(data) {
  const {
    project,
    expenses,
    performers,
    subtaskCategories,
  } = data;

  if (project.type !== 'agile' && project.type !== 'waterfall') {
    throw new Error('Received unknown "project.type"');
  }

  const tasks = data.tasks.filter(el => el.isActive === true);
  let payload = {};

  // Get duration
  let projectDurationInWorkingDays = 0;
  if (project.type === 'agile') {
    const tasksWithDuration = getTasksWithDuration(tasks, subtaskCategories, project);
    projectDurationInWorkingDays =
      _.reduce(tasksWithDuration, (total, task) => total + task.duration, 0);
    payload = tasksWithDuration;
    const insuranceDays = Math.ceil((project.insurance / 100) * projectDurationInWorkingDays);
    projectDurationInWorkingDays += insuranceDays;
  } else if (project.type === 'waterfall') {
    const phases = getPhases(subtaskCategories, tasks, project);
    projectDurationInWorkingDays = getWaterfallDuration(phases, subtaskCategories);
    payload = phases;
  }
  const cD = workingDaysToCalendar(projectDurationInWorkingDays);
  const projectDurationInCalendarDays = cD.calendarDays;

  // Get Expenses
  const monthlyExpenses = _.sumBy(expenses, expense => expense.cost);
  const totalExpenses = Math.ceil((monthlyExpenses / daysInAMonth) * projectDurationInCalendarDays);

  // Get performers/salaries (almost the same!)
  const involvedPerformersIds = getInvolvedPerformersIds(tasks);
  const involvedPerformers = performers
    .filter(performer => involvedPerformersIds.includes(performer.id));
  let performersWithProjectRewards;
  if (project.type === 'agile') {
    const tasksWithDuration = payload;
    involvedPerformers.forEach((performer) => {
      performer.workingDays = 0;
      tasksWithDuration.forEach((task) => {
        task.subtasks.forEach((subtask) => {
          if (subtask.performerId === performer.id && !project.disabledSubtaskCategories.includes(subtask.subtaskCategoryId)) {
            performer.workingDays += subtask.duration;
          }
        });
      });
    });
    performersWithProjectRewards =
      getPerformersWithProjectRewards(involvedPerformers, projectDurationInWorkingDays);
  } else if (project.type === 'waterfall') {
    const phases = payload;
    // Add workingDays and calendarWorkingDays to performers
    involvedPerformers.forEach((performer) => {
      performer.workingDays = 0;
      subtaskCategories.forEach((category) => {
        const phase = phases.filter(el => el.id === category.id)[0];
        const isCategoryDisabled = project.disabledSubtaskCategories.includes(category.id);
        phase.subtasks.forEach((subtask) => {
          if (subtask.performerId === performer.id && !isCategoryDisabled) {
            performer.workingDays += subtask.duration;
          }
        });
      });
      performer.workingDays += Math.ceil(performer.workingDays * (project.insurance / 100));
      const { calendarDays } = workingDaysToCalendar(performer.workingDays);
      performer.calendarDays = calendarDays;
    });
    performersWithProjectRewards = getPerformersWithProjectRewards(involvedPerformers);
  }
  const totalSalaries = _.sumBy(performersWithProjectRewards, performer => performer.reward);

  // Modificators
  const baseCost = totalExpenses + totalSalaries;
  const income = Math.ceil((project.income / 100) * baseCost);
  const taxes = Math.ceil((project.taxes / 100) * (baseCost + income));
  let salesmanCommission = 0;
  if (project.salesmanCommissionBase === 'total') {
    salesmanCommission =
      Math.ceil((project.salesmanCommission / 100) * (baseCost + income + taxes));
  } else if (project.salesmanCommissionBase === 'income') {
    salesmanCommission =
      Math.ceil((project.salesmanCommission / 100) * income);
  } else {
    throw new Error('Unknown project.salesmanCommissionBase');
  }

  return {
    project,
    duration: {
      workingDays: projectDurationInWorkingDays,
      calendarDays: projectDurationInCalendarDays,
    },
    cost: {
      total: baseCost + income + taxes + salesmanCommission,
      income,
      taxes,
      salesmanCommission,
      indirectExpenses: totalExpenses,
      salaries: totalSalaries,
    },
    additional: {
      performers: performersWithProjectRewards,
      payload,
    },
  };
}

export default getProjectEstimation;
