/* eslint-disable */

export const getSourceData = () => ({
  project: {
    name: 'Some mobile application',
    type: null,
    insurance: 0,
    income: 0,
    taxes: 0,
    salesmanCommission: 0,
    salesmanCommissionBase: 'total',
    disabledSubtaskCategories: [],
  },
  subtaskCategories: [
    {
      id: 1,
      name: 'Design',
      predecessors: [],
    },
    {
      id: 2,
      name: 'Back-end',
      predecessors: [],
    },
    {
      id: 3,
      name: 'Android',
      predecessors: [1, 2],
    },
    {
      id: 4,
      name: 'iOS',
      predecessors: [1, 2],
    },
    {
      id: 5,
      name: 'Testing',
      predecessors: [3, 4],
    },
  ],
  performers: [
    {
      id: 1,
      position: 'UI/UX Designer',
      salary: 40000,
    },
    {
      id: 2,
      position: 'Back-end Developer',
      salary: 50000,
    },
    {
      id: 3,
      position: 'Android Developer',
      salary: 45000,
    },
    {
      id: 4,
      position: 'iOS Developer',
      salary: 100000,
    },
    {
      id: 5,
      position: 'QA Specialist',
      salary: 20000,
    },
  ],
  tasks: [
    {
      id: 1,
      name: 'News section',
      isActive: true,
      subtasks: [
        {
          id: 1,
          subtaskCategoryId: 1,
          performerId: 1,
          duration: 5,
        },
        {
          id: 2,
          subtaskCategoryId: 2,
          performerId: 2,
          duration: 3,
        },
        {
          id: 3,
          subtaskCategoryId: 3,
          performerId: 3,
          duration: 7,
        },
        {
          id: 4,
          subtaskCategoryId: 4,
          performerId: 4,
          duration: 8,
        },
        {
          id: 5,
          subtaskCategoryId: 5,
          performerId: 5,
          duration: 1,
        },
      ],
    },
    {
      id: 2,
      name: 'Gallery section',
      isActive: true,
      subtasks: [
        {
          id: 1,
          subtaskCategoryId: 1,
          performerId: 1,
          duration: 4,
        },
        {
          id: 2,
          subtaskCategoryId: 2,
          performerId: 2,
          duration: 2,
        },
        {
          id: 3,
          subtaskCategoryId: 3,
          performerId: 3,
          duration: 6,
        },
        {
          id: 4,
          subtaskCategoryId: 4,
          performerId: 4,
          duration: 7,
        },
        {
          id: 5,
          subtaskCategoryId: 5,
          performerId: 5,
          duration: 2,
        },
      ],
    },
    {
      id: 3,
      name: 'Payments (disabled)',
      isActive: false,
      subtasks: [
        {
          id: 1,
          subtaskCategoryId: 1,
          performerId: 1,
          duration: 2048,
        },
      ],
    },
  ],
  expenses: [
    {
      id: 1,
      name: 'Office',
      cost: 10000,
    },
    {
      id: 1,
      name: 'Software licences',
      cost: 3000,
    },
  ],
});
