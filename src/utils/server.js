const feathers = require('@feathersjs/feathers');
const rest = require('@feathersjs/rest-client');
const auth = require('@feathersjs/authentication-client');
const axios = require('axios');

const app = feathers();
const restClient = rest('/api');
app.configure(restClient.axios(axios));
app.configure(auth({
  storageKey: 'accessToken',
  storage: window.localStorage,
}));

export default app;
