/* eslint-disable */

import { Selector, ClientFunction } from 'testcafe';
import settings from './utils/settings';

const { testId, baseUrl } = settings;

const getWindowLocation = ClientFunction(() => window.location.href);

fixture('Sign up').page(`${baseUrl}/sign-up`);

test('Redirects to "Create company" after success', async t => {
  await t
    .typeText('#email', `test-${testId}@example.com`)
    .typeText('#password', '1234567')
    .click('.ant-checkbox-input')
    .click('button[type="submit"]');
  await t
    .expect(getWindowLocation())
    .eql(`${baseUrl}/create-company`);
});
