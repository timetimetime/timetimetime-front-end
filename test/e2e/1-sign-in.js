/* eslint-disable */

import { Selector, ClientFunction } from 'testcafe';
import settings from './utils/settings';

const { testId, baseUrl } = settings;

const getWindowLocation = ClientFunction(() => window.location.href);

fixture('Sign in').page(`${baseUrl}/sign-in`);

test('Redirects to "Enter" after success', async t => {
  await t
    .typeText('#email', `test-${testId}@example.com`)
    .typeText('#password', '1234567')
    .click('#submit');
  await t
    .expect(getWindowLocation())
    .eql(`${baseUrl}/enter`);
});
