/* eslint-disable */

import { Selector, ClientFunction } from 'testcafe';
import settings from './utils/settings';
import tester from './utils/tester-role';

const { testId, baseUrl } = settings;

const getWindowLocation = ClientFunction(() => window.location.href);

fixture('Create company').page(`${baseUrl}/create-company`);

test('Redirects to company after success', async t => {
  await t
    .useRole(tester)
    .typeText('#name', `company-${testId}`)
    .click('button[type="submit"]');
  await t
    .expect(getWindowLocation())
    .eql(`${baseUrl}/company-${testId}`);
});
