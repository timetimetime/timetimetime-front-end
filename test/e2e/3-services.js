/* eslint-disable */

import { Selector } from 'testcafe';
import settings from './utils/settings';
import tester from './utils/tester-role';

const { testId, baseUrl } = settings;
const url = `${baseUrl}/company-${testId}/product-types`;

fixture('Services').page(url);

test('Can create model', async t => {
  await t
    .useRole(tester)
    .navigateTo(url)
    .typeText('#create-model-form #name', `Mobile application`)
    .click('#create-model-form button[type="submit"]');
});
