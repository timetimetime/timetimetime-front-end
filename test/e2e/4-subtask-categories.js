/* eslint-disable */

import { Selector } from 'testcafe';
import settings from './utils/settings';
import tester from './utils/tester-role';

const { testId, baseUrl } = settings;
const url = `${baseUrl}/company-${testId}/work-categories`;

fixture('Subtask categories').page(url);

test('Can create models', async t => {
  await t
    .useRole(tester)
    .navigateTo(url)
    // Disign
    .typeText('#create-model-form #name', `Design`)
    .click('#create-model-form button[type="submit"]')
    // Back-end
    .typeText('#create-model-form #name', `Back-end`)
    .click('#create-model-form button[type="submit"]')
    // Android
    .typeText('#create-model-form #name', `Android`)
    .click('#create-model-form .ant-select')
    .click('.predecessors-select li:nth-child(1)')
    .click('.predecessors-select li:nth-child(2)')
    .click('body')
    .click('#create-model-form button[type="submit"]')
    // iOS
    .typeText('#create-model-form #name', `iOS`)
    .click('#create-model-form .ant-select')
    .click('.predecessors-select li:nth-child(1)')
    .click('.predecessors-select li:nth-child(2)')
    .click('body')
    .click('#create-model-form button[type="submit"]')
    // Testing
    .typeText('#create-model-form #name', `Testing`)
    .click('#create-model-form .ant-select')
    .click('.predecessors-select li:nth-child(3)')
    .click('.predecessors-select li:nth-child(4)')
    .click('body')
    .click('#create-model-form button[type="submit"]')
});
