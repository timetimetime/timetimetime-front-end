/* eslint-disable */

import settings from './utils/settings';
import tester from './utils/tester-role';

const { testId, baseUrl } = settings;
const url = `${baseUrl}/company-${testId}/performers`;

fixture('Performers').page(url);

test('Can create models', async t => {
  await t
    .useRole(tester)
    .navigateTo(url)
    // Designer
    .typeText('#firstName', `Designer`)
    .typeText('#lastName', `Designer`)
    .click('.ant-select')
    .click('.position-select li:nth-child(1)')
    .click('body')
    .typeText('#salary', `40000`)
    .click('button[type="submit"]')
    // Back-ender
    .typeText('#firstName', `Back-ender`)
    .typeText('#lastName', `Back-ender`)
    .click('.ant-select')
    .click('.position-select li:nth-child(2)')
    .click('body')
    .typeText('#salary', `50000`)
    .click('button[type="submit"]')
    // Androider
    .typeText('#firstName', `Androider`)
    .typeText('#lastName', `Androider`)
    .click('.ant-select')
    .click('.position-select li:nth-child(3)')
    .click('body')
    .typeText('#salary', `45000`)
    .click('button[type="submit"]')
    // iOSer
    .typeText('#firstName', `iOSer`)
    .typeText('#lastName', `iOSer`)
    .click('.ant-select')
    .click('.position-select li:nth-child(4)')
    .click('body')
    .typeText('#salary', `100000`)
    .click('button[type="submit"]')
    // Tester
    .typeText('#firstName', `Tester`)
    .typeText('#lastName', `Tester`)
    .click('.ant-select')
    .click('.position-select li:nth-child(5)')
    .click('body')
    .typeText('#salary', `20000`)
    .click('button[type="submit"]')
});
