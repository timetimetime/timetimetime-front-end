/* eslint-disable */

import settings from './utils/settings';
import tester from './utils/tester-role';

const { testId, baseUrl } = settings;
const url = `${baseUrl}/company-${testId}/expenses`;

fixture('Expenses').page(url);

test('Can create models', async t => {
  await t
    .useRole(tester)
    .navigateTo(url)
    // Office
    .typeText('#name', `Office`)
    .typeText('#cost', `10000`)
    .click('button[type="submit"]')
    // Pizza
    .typeText('#name', `Pizza`)
    .typeText('#cost', `3000`)
    .click('button[type="submit"]');
});
