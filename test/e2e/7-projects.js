/* eslint-disable */

import settings from './utils/settings';
import tester from './utils/tester-role';

const { testId, baseUrl } = settings;
const url = `${baseUrl}/company-${testId}`;

fixture('Projects').page(url);

test('Can create models', async t => {
  await t
    .useRole(tester)
    .navigateTo(url)
    .typeText('#name', `Photo agency app`)
    .click('button[type="submit"]');
});
