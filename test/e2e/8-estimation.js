/* eslint-disable */

import { Selector } from 'testcafe';
import settings from './utils/settings';
import tester from './utils/tester-role';

const { testId, baseUrl } = settings;
const url = `${baseUrl}/company-${testId}/photo-agency-app`;

fixture('Estimation').page(url);

test('Can create models', async t => {
  await t
    .useRole(tester)
    .navigateTo(url)
    // CASE: WATERFALL WITHOUT MODIFICATORS
    // Add tasks
    .click('#add-tasks-btn')
    .typeText('input[id="tasks[0].name"]', `News section`)
    .typeText('input[id="tasks[1].name"]', `Gallery section`)
    .typeText('input[id="tasks[2].name"]', `Something stupid and useless`)
    .click('.ant-modal-footer .ant-btn-primary')
    // Add subtasks for news section
    .typeText('div[data-task-name="News section"] div[data-category="Design"] input', '5')
    .typeText('div[data-task-name="News section"] div[data-category="Back-end"] input', '3')
    .typeText('div[data-task-name="News section"] div[data-category="Android"] input', '7')
    .typeText('div[data-task-name="News section"] div[data-category="iOS"] input', '8')
    .typeText('div[data-task-name="News section"] div[data-category="Testing"] input', '1')
    .click('div[data-task-name="News section"] button[type="submit"]')
    // Add subtasks for gallery section
    .typeText('div[data-task-name="Gallery section"] div[data-category="Design"] input', '4')
    .typeText('div[data-task-name="Gallery section"] div[data-category="Back-end"] input', '2')
    .typeText('div[data-task-name="Gallery section"] div[data-category="Android"] input', '6')
    .typeText('div[data-task-name="Gallery section"] div[data-category="iOS"] input', '7')
    .typeText('div[data-task-name="Gallery section"] div[data-category="Testing"] input', '2')
    .click('div[data-task-name="Gallery section"] button[type="submit"]')
    // Set waterfall
    .click('#project-settings .ant-select')
    .click('.project-type-select li:nth-child(2)')
    .click('#project-settings button[type="submit"]')
    // Get estimation
    .click('#get-calculation-button');
  const totalCost = Selector('#total-cost');
  const totalDuration = Selector('#total-duration');
  await t
    .expect(totalDuration.textContent)
    .eql('27');
  await t
    .expect(totalCost.textContent)
    .eql('141,263');
  // CASE: WATERFALL WITH MODIFICATORS
  await t
    .typeText('#insurance', '100')
    .typeText('#income', '80')
    .typeText('#taxes', '25')
    .typeText('#salesmanCommission', '5')
    .click('#project-settings button[type="submit"]');
  await t
    .expect(totalDuration.textContent)
    .eql('54');
  await t
    .expect(totalCost.textContent)
    .eql('667,464');
  // CASE: AGILE WITH MODIFICATORS
  await t
    .click('#project-settings .ant-select')
    .click('.project-type-select li:nth-child(1)')
    .click('#project-settings button[type="submit"]');
  await t
    .expect(totalDuration.textContent)
    .eql('54');
  await t
    .expect(totalCost.textContent)
    .eql('1,554,475');
  // CASE: AGILE WITHOUT MODIFICATORS
  await t
    .typeText('#insurance', '0', { replace: true })
    .typeText('#income', '0', { replace: true })
    .typeText('#taxes', '0', { replace: true })
    .typeText('#salesmanCommission', '0', { replace: true })
    .click('#project-settings button[type="submit"]');
  await t
    .expect(totalDuration.textContent)
    .eql('27');
  await t
    .expect(totalCost.textContent)
    .eql('328,991');
});
