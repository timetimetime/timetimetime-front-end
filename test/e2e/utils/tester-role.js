/* eslint-disable */

import { Role } from 'testcafe';
import settings from './settings';

const { testId, baseUrl } = settings;

export default Role(`${baseUrl}/sign-in`, async t => {
  await t
    .typeText('#email', `test-${testId}@example.com`)
    .typeText('#password', '1234567')
    .click('#submit');
});
